using namespace std;
#include<iostream>
#include<math.h>
#include<stdlib.h>
#include<fstream>
#include<sstream>
#include<vector>
#include<ctime>
#include <unistd.h>


double distance(double v1[9], double v2[9]);


int main(int argc, char* argv[]){
  //ARGV[1] = timestep
  //ARGV[2] = infile-no time
  //ARGV[3] = outfile-no time
 
  srand(time(NULL));

  int time=atoi(argv[1]);
  string infile_old=argv[2];
  string outfile_new=argv[3];

  double Lx,Ly,Lz;
  double Lmaxx,Lminx,
         Lmaxy,Lminy,
         Lmaxz,Lminz;

  int m;   // mass
  int Ntotal,ntypes;
  int nbonds,nbondtypes;
  int nangles,nangletypes;
  int ndihedrals, ndihedraltypes;
  int id,mol,type;
  double xx,yy,zz;
  int ix,iy,iz;
  double vx,vy,vz;
  int bead1,bead2,bead3, bead4, bead5, bead6;

  string dummy;


  // READ FILE
  ifstream indata;
  stringstream readFile;
  readFile.str("");
  readFile.clear();
  readFile << infile_old << time;
  indata.open(readFile.str().c_str());
  cout << readFile.str().c_str()<<endl;
  if(!indata.good()){cout << "Error while opening data file!"<<endl; cin.get();cin.get();cin.get();}

  // read 11 lines
  for(int i=0;i<16;i++){
    if(i==2) {
      indata >> Ntotal;
      cout << "Ntotal: " << Ntotal <<endl;
    }
    if(i==3){
      indata >> ntypes;
      cout << "Ntypes: " << ntypes <<endl;
    }
    if(i==4){
      indata >> nbonds;
      cout << "Nbonds: " << nbonds <<endl;
    }
    if(i==5){
      indata >> nbondtypes;
      cout << "Nbondtypes: " << nbondtypes <<endl;
    }
    if(i==6){
      indata >> nangles;
      cout << "Nangles: " << nangles <<endl;
    }
    if(i==7){
      indata >> nangletypes;
      cout << "Nangletypes: " << nangletypes <<endl;
    }
    if(i==8){
      indata >> ndihedrals;
      cout << "Ndihedrals: " << ndihedrals <<endl;
    }
    if(i==9){
      indata >> ndihedraltypes;
      cout << "Ndihedraltypes: " << ndihedraltypes <<endl;
    }
    if(i==11) {
      indata >> Lminx >> Lmaxx;
      cout << "L " << Lminx << " " << Lmaxx <<endl;
      Lx = Lmaxx-Lminx;
      cout << "Lx: " << Lx <<endl;
    }
    if(i==12) {
      indata >> Lminy >> Lmaxy;
      cout << "L " << Lminy << " " << Lmaxy <<endl;
      Ly = Lmaxy-Lminy;
      cout << "Ly: " << Ly <<endl; 
    }
    if(i==13) {
      indata >> Lminz >> Lmaxz;
      cout << "L " << Lminz << " " << Lmaxz <<endl;
      Lz = Lmaxz-Lminz;
      cout << "Lz: " << Lz <<endl;
    }
    else getline(indata,dummy);
  }


  vector<int> mass(ntypes);                                                  
  vector< vector<int> > Atom1(6, vector<int>(Ntotal));                  // id,mol,type,ix,iy,iz
  vector< vector<double> > Atom2(3, vector<double>(Ntotal));            // x,y,z
  vector< vector<double> > upos(3, vector<double>(Ntotal));             //unwrapped position
  vector< vector<double> > Velocity(3, vector<double>(Ntotal));         // id, vx, vy, vz
  
  int bond1, bond2;
  vector< vector<int> > Bond(3, vector<int>(nbonds));                   // bond type, idbond1, idbond2 --> All of the bonds
  int nbeads = Ntotal/4;
  //These are always the same length
  vector< vector<int> > Fene(3, vector<int>(2*(nbeads-1)));                 //The fene bonds 
  vector< vector<int> > HB(3, vector<int>(nbeads));                     //The hydrogen bonds 
  vector< vector<int> > HB_order(3, vector<int>(nbeads));               //The hydrogen bonds ordered 
  vector<double> magnitude_HB(nbeads);                                  //Magnitude of the HB (double because stores a distance) 
  vector<int> Denaturation(nbeads);                                     //Denaturation depends on the HB 
  //This change the length
  vector< vector<int> > Stacking(3);                                    //The stacking bonds
  vector< vector<int> > id_create_stacking_s1(3);    
  vector< vector<int> > id_create_stacking_s2(3);


  int angle1, angle2, angle3;
  vector< vector<int> > Angle(4, vector<int>(nangles));                 // angle type, idangle1, idangle2, id angle3 --> All of the angles
  // We don't kno the length of these vectors
  vector< vector<int> > Roll(4);                                        //Roll (P-P-B) angle
  vector< vector<int> > Cosine_ds(4);                                   //Persistence length (P-P-P) angle of the dsSNA
  vector< vector<int> > Cosine_ss(4);                                   //Persistence length (P-P-P) angle of the ssSNA

  vector< vector<int> > id_create_roll_s1(4);    
  vector< vector<int> > id_create_roll_s2(4);

  vector< vector<int> > id_create_cosine_s1(4);   
  vector< vector<int> > id_create_cosine_s2(4);   

  int dihedral1, dihedral2, dihedral3, dihedral4;
  vector< vector<int> > Dihedral(5, vector<int>(ndihedrals));           // dihedral type, iddihedral1, iddihedral2, iddihedral3, iddihedral4 --> All of the dihedrals
  vector< vector<int> > id_create_dihedral_s1(5);    
  vector< vector<int> > id_create_dihedral_s2(5);


  // Read masses 
  getline(indata,dummy);
  for(int n=0;n<ntypes; n++){indata >> m >> mass[n];}

  // Read empty lines
  for(int n=0;n<3; n++){getline(indata,dummy);}   // Atoms # molecular
                                                
  // READ ATOMS and order them 
  for(int n=0; n<Ntotal; n++){
    indata >> id >> mol >> type >> xx >> yy >> zz >> ix >> iy >> iz;
//    indata >> id >> mol >> type >> xx >> yy >> zz;


    Atom1[0][id-1] = id;
    Atom1[1][id-1] = mol;
    Atom1[2][id-1] = type;

    Atom2[0][id-1] = xx;
    Atom2[1][id-1] = yy;    /*x, y and z position of both, patches and beads in the two strands*/
    Atom2[2][id-1] = zz;

    Atom1[3][id-1] = ix;
    Atom1[4][id-1] = iy;
    Atom1[5][id-1] = iz;
    
    upos[0][id-1] = xx+Lx*ix;
    upos[1][id-1] = yy+Ly*iy;       /*Unwrapped coordinates*/
    upos[2][id-1] = zz+Lz*iz;
  }
  //for(int n=0; n<Ntotal; n++){cout << "id: " << Atom1[0][n] << endl;}

  // READ VELOCITIES
  indata >> dummy;
  for(int n=0; n<Ntotal; n++){
    indata >> id >> vx >> vy >> vz;
    Velocity[0][id-1]=vx;
    Velocity[1][id-1]=vy;
    Velocity[2][id-1]=vz;
  }

  // READ BONDS
  indata >> dummy;
  for(int n=0; n<nbonds; n++){
    indata >> id >> type >> bond1 >> bond2;
    Bond[0][n]=type;
    Bond[1][n]=bond1;
    Bond[2][n]=bond2;
  }
  //for(int n=0; n<nbonds; n++){cout << "Bond (type - 1 - 2): " << Bond[0][n] << " " << Bond[1][n] << " " << Bond[2][n] <<  endl;}

 // READ ANGLES
  indata >> dummy;
  for(int n=0; n<nangles; n++){
    indata >> id >> type >> angle1 >> angle2 >> angle3;
    Angle[0][n]=type;
    Angle[1][n]=angle1;
    Angle[2][n]=angle2;
    Angle[3][n]=angle3;    
  }
  //for(int n=0; n<nangles; n++){cout << "Angle (type - 1 - 2 - 3): " << Angle[0][n] << " " << Angle[1][n] << " " << Angle[2][n] <<" " << Angle[3][n] <<  endl;}

  // READ DIHEDRALS
  indata >> dummy;
  for(int n=0; n<ndihedrals; n++){
    indata >> id >> type >> dihedral1 >> dihedral2 >> dihedral3 >> dihedral4;
    Dihedral[0][n]=type;
    Dihedral[1][n]=dihedral1;
    Dihedral[2][n]=dihedral2;
    Dihedral[3][n]=dihedral3;
    Dihedral[4][n]=dihedral4;
  }
  //for(int n=0; n<ndihedrals; n++){cout << "Dihedral (type - 1 - 2 - 3 - 4): " << Dihedral[0][n] << " " << Dihedral[1][n] << " " << Dihedral[2][n] <<" " << Dihedral[3][n] << " " << Dihedral[4][n] <<  endl;}
  
  // ORDERING ANGLES BY TYPE

  for(int n=0; n<nangles; n++){
      //PPB
      if(Angle[0][n]==1){
          int ida1 = Angle[1][n];
          int ida2 = Angle[2][n];       
          int ida3 = Angle[3][n];       
          
          Roll[0].push_back (1);
          Roll[1].push_back (ida1);
          Roll[2].push_back (ida2);
          Roll[3].push_back (ida3);
      }
     
     //PPP dsDNA
      if(Angle[0][n]==2){
          int idp1 = Angle[1][n];
          int idp2 = Angle[2][n];       
          int idp3 = Angle[3][n];       
          
          Cosine_ds[0].push_back (2);
          Cosine_ds[1].push_back (idp1);
          Cosine_ds[2].push_back (idp2);
          Cosine_ds[3].push_back (idp3);
      }

      //PPP ssDNA
      if(Angle[0][n]==3){
          int idp1ss = Angle[1][n];
          int idp2ss = Angle[2][n];       
          int idp3ss = Angle[3][n];       
          
          Cosine_ss[0].push_back (3);
          Cosine_ss[1].push_back (idp1ss);
          Cosine_ss[2].push_back (idp2ss);
          Cosine_ss[3].push_back (idp3ss);
      }
  }
  //for(int n=0; n<2*nbeads; n++){cout << "Roll: " << Roll[0][n] << " " << Roll[1][n] << " " << Roll[2][n] << " " << Roll[3][n] << endl;}
  //for(int n=0; n<Cosine_ds[0].size(); n++){cout << "Cosine dsDNA: " << Cosine_ds[0][n] << " " << Cosine_ds[1][n] << " " << Cosine_ds[2][n] << " " << Cosine_ds[3][n] << endl;}
  //for(int n=0; n<Cosine_ss[0].size(); n++){cout << "Cosine ssDNA: " << Cosine_ss[0][n] << " " << Cosine_ss[1][n] << " " << Cosine_ss[2][n] << " " << Cosine_ss[3][n] << endl;}

  // ORDERING BONDS BY TYPE
  int count1 = 0;
  int count2 = 0;

  for(int n=0; n<nbonds; n++){
      //Fene bonds of the two strands
      if(Bond[0][n]==1){
          int idb1 = Bond[1][n];
          int idb2 = Bond[2][n];       
          
          Fene[0][count1] = 1;
          Fene[1][count1] = idb1;
          Fene[2][count1] = idb2;

          count1 = count1 + 1;
      }
      //Hydrogen bonds
      if(Bond[0][n]==2){
          int idp1 = Bond[1][n];
          int idp2 = Bond[2][n];       
          
          HB[0][count2] = 2;
          HB[1][count2] = idp1;
          HB[2][count2] = idp2;

          count2 = count2 + 1;
      }
      //Stacking bonds of the two strands
      if(Bond[0][n]==3){
          int idc1 = Bond[1][n];
          int idc2 = Bond[2][n];       
          
          Stacking[0].push_back (3);
          Stacking[1].push_back (idc1);
          Stacking[2].push_back (idc2);
      }
  }
  //for(int n=0; n<2*nbeads; n++){cout << "Fene bond: " << Fene[0][n] << " " << Fene[1][n] << " " << Fene[2][n] << endl;}
  //for(int n=0; n<nbeads; n++){cout << "HB: " << HB[0][n] << " " << HB[1][n] << " " << HB[2][n] << endl;}
  //for(int n=0; n<Stacking[0].size(); n++){cout << "Stacking: " << Stacking[0][n] << " " << Stacking[1][n] << " " << Stacking[2][n] << endl;}


 
  // ORDERING HYDROGEN BONDS
  for(int n=0; n<nbeads; n++)
  {
      int idp1 = HB[1][n];
      int idp2 = HB[2][n];       

      for(int j=0; j<nbeads; j++)
      {
          if ((idp1 == 2*(j+1)))
          {
                  HB_order[0][j] = 2;  
                  HB_order[1][j] = idp1;
                  HB_order[2][j] = idp2;
          }
      }
  }
  //for(int n=0; n<nbeads; n++){cout << "HB order: " << HB_order[0][n] << " " << HB_order[1][n] << " " << HB_order[2][n] << " " << endl;}

  // MAGNITUDE OF HYDROGEN BONDS
  int nden=0;
  for(int n=0; n<nbeads; n++){
          int idp1 = HB_order[1][n];
          int idp2 = HB_order[2][n];

          double dist = sqrt(pow((upos[0][idp2-1]-upos[0][idp1-1]),2) + pow((upos[1][idp2-1]-upos[1][idp1-1]),2) + pow((upos[2][idp2-1]-upos[2][idp1-1]),2));  

          magnitude_HB[n] = dist;
          
          if(dist<=0.3){
            Denaturation[n]=1;
          }

          if(dist>0.3){
            Denaturation[n]=0;
            nden = nden+1;
          }                    
  }
//  for(int n=0; n<nbeads; n++){cout << n+1 << " Magnitude of HB: " << magnitude_HB[n] << endl;}
//  for(int n=0; n<nbeads; n++){cout << n+1 << " Denaturation: " << Denaturation[n] << endl;}


  // HOW MANY TIMES THERE ARE 2 CONSECUTIVE DENATURATED BP
  int n2consden = 0;
  for(int n=0; n<nbeads-1; n++){
      
      if(Denaturation[n]==0 && Denaturation[n+1]==0)
      {
          n2consden = n2consden + 1;
      }
  }
  cout <<"Number of denaturated bp: " << nden << endl;
  cout <<"Number of 2 consecutive denaturated bp: " << n2consden << endl;


 // SAVE THE ID OF THE 4 PARTICLES IN THE DIHEDRAL OF THE TWO CONSECUTIVE DENATURATED BP
 // AND THE ID OF THE 2 PATCHES IN THE MORSE INTERACTION OF THE TWO CONSECUTIVE DENATURATED BP
 // AND THE ID OF THE 2 PATCHES AND 1 BEAD IN THE ROLL INTERACTION OF THE TWO CONSECUTIVE DENATURATED BP
 

  //DIHEDRALS
  for(int n=0; n<nbeads-1; n++)
  {
      int contador1 = Denaturation[n] + Denaturation[n+1]; //2 for 0 denaturated bp
                                                           //1 for 1 denaturated bp
                                                           //0 for 2 denaturated bp
      //NON DENATURATED DIHEDRALS
      if(contador1>0)
      {    
          //FOR STRAND 1
          id_create_dihedral_s1[0].push_back (HB_order[1][n]-1);   //id of the first bead in the dihedral
          id_create_dihedral_s1[1].push_back (HB_order[1][n]);     //id of the first patch in the dihedral
          id_create_dihedral_s1[2].push_back (HB_order[1][n+1]);   //id of the second patch in the dihedral
          id_create_dihedral_s1[3].push_back (HB_order[1][n+1]-1); //id of the second bead in the dihedral
          id_create_dihedral_s1[4].push_back (1);                  //type of the dihedral

          //FOR STRAND 2
          id_create_dihedral_s2[0].push_back (HB_order[2][n]-1);   
          id_create_dihedral_s2[1].push_back (HB_order[2][n]);     
          id_create_dihedral_s2[2].push_back (HB_order[2][n+1]);   
          id_create_dihedral_s2[3].push_back (HB_order[2][n+1]-1);             
          id_create_dihedral_s2[4].push_back (1);                  //type of the dihedral      
      }

      //DENATURATED DIHEDRALS
      if(contador1==0)
      {    
          //FOR STRAND 1
          id_create_dihedral_s1[0].push_back (HB_order[1][n]-1);   
          id_create_dihedral_s1[1].push_back (HB_order[1][n]);     
          id_create_dihedral_s1[2].push_back (HB_order[1][n+1]);   
          id_create_dihedral_s1[3].push_back (HB_order[1][n+1]-1); 
          id_create_dihedral_s1[4].push_back (2);                  //type of the dihedral for the denaturated bp

          //FOR STRAND 2
          id_create_dihedral_s2[0].push_back (HB_order[2][n]-1);   
          id_create_dihedral_s2[1].push_back (HB_order[2][n]);     
          id_create_dihedral_s2[2].push_back (HB_order[2][n+1]);   
          id_create_dihedral_s2[3].push_back (HB_order[2][n+1]-1);             
          id_create_dihedral_s2[4].push_back (2);                  //type of the dihedral for the denaturated bp
      }

      else;
  }


  

  //STACKING
  for(int n=0; n<nbeads-1; n++)
  {
      int contador3 = Denaturation[n] + Denaturation[n+1]; //2 for 0 denaturated bp
                                                           //1 for 1 denaturated bp
                                                           //0 for 2 denaturated bp

      //NON DENATURATED STACKING
      if(contador3>0)
      {    
           //FOR STRAND 1
           id_create_stacking_s1[0].push_back (HB_order[1][n]);     //id of the first patch in the stacking
           id_create_stacking_s1[1].push_back (HB_order[1][n+1]);   //id of the second patch in the stacking
           id_create_stacking_s1[2].push_back (3);                  //type of the Stacking bond

           //FOR STRAND 2
           id_create_stacking_s2[0].push_back (HB_order[2][n]);     
           id_create_stacking_s2[1].push_back (HB_order[2][n+1]);   
           id_create_stacking_s2[2].push_back (3);   
     }


      //DENATURATED STACKING
      if(contador3==0)
      {    
           //FOR STRAND 1
           id_create_stacking_s1[0].push_back (HB_order[1][n]);  
           id_create_stacking_s1[1].push_back (HB_order[1][n+1]);
           id_create_stacking_s1[2].push_back (4);                  //type of the Stacking for denaturated bp

           //FOR STRAND 2
           id_create_stacking_s2[0].push_back (HB_order[2][n]);     
           id_create_stacking_s2[1].push_back (HB_order[2][n+1]);   
           id_create_stacking_s2[2].push_back (4);                  //type of the Stacking for denaturated bp   
      }

      else;
  } 





  //ROLL
  for(int n=0; n<nbeads-1; n++)
  {
      if(n>0 && n<nbeads-2)
      {
          int contador5 = Denaturation[n] + Denaturation[n+1]; //2 for 0 denaturated bp
                                                               //1 for 1 denaturated bp
                                                               //0 for 2 denaturated bp

          //NON DENATURATED ROLL
          if(contador5>0)
          {    
              //FOR STRAND 1
              id_create_roll_s1[0].push_back (HB_order[1][n]);     //id of the first patch in the roll angle
              id_create_roll_s1[1].push_back (HB_order[1][n+1]);   //id of the second patch in the roll angle
              id_create_roll_s1[2].push_back (HB_order[1][n+1]-1); //id of the bead in the roll angle   
              id_create_roll_s1[3].push_back (1);                  //type of the roll angle   

              //FOR STRAND 2
              id_create_roll_s2[0].push_back (HB_order[2][n+1]);     
              id_create_roll_s2[1].push_back (HB_order[2][n]);   
              id_create_roll_s2[2].push_back (HB_order[2][n]-1);   
              id_create_roll_s2[3].push_back (1);                  //type of the roll angle   
          }

          //DENATURATED ROLL
          if(contador5==0)
          {    
              //FOR STRAND 1
              id_create_roll_s1[0].push_back (HB_order[1][n]);     
              id_create_roll_s1[1].push_back (HB_order[1][n+1]);   
              id_create_roll_s1[2].push_back (HB_order[1][n+1]-1); 
              id_create_roll_s1[3].push_back (3);                  //type of the roll angle for the denaturated bp

              //FOR STRAND 2
              id_create_roll_s2[0].push_back (HB_order[2][n+1]);     
              id_create_roll_s2[1].push_back (HB_order[2][n]);   
              id_create_roll_s2[2].push_back (HB_order[2][n]-1);   
              id_create_roll_s2[3].push_back (3);                  //type of the roll angle for the denaturated bp 
          }
          else;
        }

        //For the beginning of the linear molecule where we have the constriction in strand 1
        if(n==0)
        {
          int contador6 = Denaturation[0] + Denaturation[1];

          //NON DENATURATED ROLL
          if(contador6>0)
          {
              //FOR STRAND 1 creates the patch-patch-bead angle (2-4-3)
              id_create_roll_s1[0].push_back (HB_order[1][n]);     
              id_create_roll_s1[1].push_back (4);   
              id_create_roll_s1[2].push_back (3);
              id_create_roll_s1[3].push_back (1);                  //type of the roll angle         

              //FOR STRAND 1 creates the constriction bead-patch-patch angle (1-2-4)
              id_create_roll_s1[0].push_back (1);     
              id_create_roll_s1[1].push_back (2);   
              id_create_roll_s1[2].push_back (4);
              id_create_roll_s1[3].push_back (1);                  //type of the roll angle         


              //FOR STRAND 2 creates the patch-patch-bead angle
              id_create_roll_s2[0].push_back (2*nbeads+4);     
              id_create_roll_s2[1].push_back (2*nbeads+2);   
              id_create_roll_s2[2].push_back (2*nbeads+1);   
              id_create_roll_s2[3].push_back (1);                  //type of the roll angle   
              
               
              /*This time we don't create the roll angle constriction in this position*/
              //FOR STRAND 2 creates the constriction bead-patch-patch angle 
              /*id_create_roll_s2[0].push_back (2*nbeads+1);     
              id_create_roll_s2[1].push_back (2*nbeads+2);   
              id_create_roll_s2[2].push_back (2*nbeads+4);
              id_create_roll_s2[3].push_back (1);         */
          }

          //DENATURATED ROLL
          if(contador6==0)
          {
              //FOR STRAND 1 creates the patch-patch-bead angle (2-4-3)
              id_create_roll_s1[0].push_back (HB_order[1][n]);     
              id_create_roll_s1[1].push_back (4);   
              id_create_roll_s1[2].push_back (3);
              id_create_roll_s1[3].push_back (3);                  //type of the roll angle         

              //FOR STRAND 1 creates the constriction bead-patch-patch angle (1-2-4)
              id_create_roll_s1[0].push_back (1);     
              id_create_roll_s1[1].push_back (2);   
              id_create_roll_s1[2].push_back (4);
              id_create_roll_s1[3].push_back (3);                  //type of the roll angle         


              //FOR STRAND 2 creates the patch-patch-bead angle
              id_create_roll_s2[0].push_back (2*nbeads+4);     
              id_create_roll_s2[1].push_back (2*nbeads+2);   
              id_create_roll_s2[2].push_back (2*nbeads+1);   
              id_create_roll_s2[3].push_back (3);                  //type of the roll angle    

              /*This time we don't create the roll angle constriction in this position*/
              //FOR STRAND 2 creates the constriction bead-patch-patch angle 
              /*id_create_roll_s2[0].push_back (2*nbeads+1);     
              id_create_roll_s2[1].push_back (2*nbeads+2);   
              id_create_roll_s2[2].push_back (2*nbeads+4);
              id_create_roll_s2[3].push_back (3);*/                  //type of the roll angle          
          }

          else;
      }
      
      //For the end of the linear molecule where we have the constriction in strand 2
      if(n==nbeads-2)
      {
          int contador6 = Denaturation[nbeads-2] + Denaturation[nbeads-1];

          //NON DENATURATED ROLL
          if(contador6>0)
          {
              //FOR STRAND 1 creates the patch-patch-bead angle
              id_create_roll_s1[0].push_back (HB_order[1][n]);     
              id_create_roll_s1[1].push_back (HB_order[1][n+1]);   
              id_create_roll_s1[2].push_back (HB_order[1][n+1]-1);
              id_create_roll_s1[3].push_back (1);                  //type of the roll angle         

                     

              //FOR STRAND 2 creates the patch-patch-bead angle
              id_create_roll_s2[0].push_back (HB_order[2][n+1]);     
              id_create_roll_s2[1].push_back (HB_order[2][n]);   
              id_create_roll_s2[2].push_back (HB_order[2][n]-1);   
              id_create_roll_s2[3].push_back (1);                  //type of the roll angle   
              
               
              //FOR STRAND 2 creates the constriction bead-patch-patch angle 
              id_create_roll_s2[0].push_back (HB_order[2][n+1]-1);     
              id_create_roll_s2[1].push_back (HB_order[2][n+1]);   
              id_create_roll_s2[2].push_back (HB_order[2][n]);
              id_create_roll_s2[3].push_back (1);         
          }

          //DENATURATED ROLL
          if(contador6==0)
          {
              //FOR STRAND 1 creates the patch-patch-bead angle
              id_create_roll_s1[0].push_back (HB_order[1][n]);     
              id_create_roll_s1[1].push_back (HB_order[1][n+1]);   
              id_create_roll_s1[2].push_back (HB_order[1][n+1]-1);
              id_create_roll_s1[3].push_back (3);                  //type of the roll angle         

                     

              //FOR STRAND 2 creates the patch-patch-bead angle
              id_create_roll_s2[0].push_back (HB_order[2][n+1]);     
              id_create_roll_s2[1].push_back (HB_order[2][n]);   
              id_create_roll_s2[2].push_back (HB_order[2][n]-1);   
              id_create_roll_s2[3].push_back (3);                  //type of the roll angle   
              
               
              //FOR STRAND 2 creates the constriction bead-patch-patch angle 
              id_create_roll_s2[0].push_back (HB_order[2][n+1]-1);     
              id_create_roll_s2[1].push_back (HB_order[2][n+1]);   
              id_create_roll_s2[2].push_back (HB_order[2][n]);
              id_create_roll_s2[3].push_back (3);        
          }

          else;
      }
  }      

//for(int n=0; n<id_create_dihedral_s1[0].size(); n++){cout <<"Id of the dihedrals: " << n+1 << " " << id_create_dihedral_s1[4][n] <<" " << id_create_dihedral_s1[0][n] <<" "<< id_create_dihedral_s1[1][n] <<" "<< id_create_dihedral_s1[2][n] <<" "<< id_create_dihedral_s1[3][n]<<endl;}
//for(int n=0; n<id_create_dihedral_s2[0].size(); n++){cout <<"Id of the dihedrals: " << n+1 << " " << id_create_dihedral_s2[4][n] <<" " << id_create_dihedral_s2[0][n] <<" "<< id_create_dihedral_s2[1][n] <<" "<< id_create_dihedral_s2[2][n] <<" "<< id_create_dihedral_s2[3][n]<<endl;}

//for(int n=0; n<id_create_stacking_s1[0].size(); n++){cout <<"Id of the stacking: " << n+1 << " " << id_create_stacking_s1[2][n] << " " << id_create_stacking_s1[0][n] <<" "<< id_create_stacking_s1[1][n] << endl;}
//for(int n=0; n<id_create_stacking_s2[0].size(); n++){cout <<"Id of the stacking: " << n+1 << " " << id_create_stacking_s2[2][n] << " " << id_create_stacking_s2[0][n] <<" "<< id_create_stacking_s2[1][n] << endl;}

//for(int n=0; n<id_create_roll_s1[0].size(); n++){cout <<"Id of the roll: " << n+1 << " " << id_create_roll_s1[3][n] << " " << id_create_roll_s1[0][n] <<" "<< id_create_roll_s1[1][n] << " " << id_create_roll_s1[2][n] <<  endl;}
//for(int n=0; n<id_create_roll_s2[0].size(); n++){cout <<"Id of the roll: " << n+1 << " " << id_create_roll_s2[3][n] << " " << id_create_roll_s2[0][n] <<" "<< id_create_roll_s2[1][n] << " " << id_create_roll_s2[2][n] <<  endl;}



/*REORDER THE ROLL ANGLE SO IT APPEARS ALL THE ROLL ANGLES IN STRAND1, THEN THE CONSTRICTION IN STRAND1, THEN ROLL ANGLES IN STRAND2, THEN CONSTRICTION IN S2*/
  vector< vector<int> > order_roll_s1(4);    
  vector< vector<int> > order_roll_s2(4);

  for(int n=0; n<id_create_roll_s1[0].size(); n++)
  {
    //Avoid constriction in the first strand (1-2-4). This is the only roll angle that starts with 1
    if(id_create_roll_s1[0][n]!=1)
    {
        order_roll_s1[0].push_back(id_create_roll_s1[0][n]);
        order_roll_s1[1].push_back(id_create_roll_s1[1][n]);
        order_roll_s1[2].push_back(id_create_roll_s1[2][n]);
        order_roll_s1[3].push_back(id_create_roll_s1[3][n]);
    }

    //Avoid constriction in the second strand (4*nbp-1 --- 4*nbp ---- 4*nbp-2). This is the only roll angle that starts with 4*nbp-1
    if(id_create_roll_s2[0][n]!=4*nbeads-1)
    {
        order_roll_s2[0].push_back(id_create_roll_s2[0][n]);
        order_roll_s2[1].push_back(id_create_roll_s2[1][n]);
        order_roll_s2[2].push_back(id_create_roll_s2[2][n]);
        order_roll_s2[3].push_back(id_create_roll_s2[3][n]);
    }


  }

  //At the end of each vector add the constriction
  order_roll_s1[0].push_back(id_create_roll_s1[0][1]);
  order_roll_s1[1].push_back(id_create_roll_s1[1][1]);
  order_roll_s1[2].push_back(id_create_roll_s1[2][1]);
  order_roll_s1[3].push_back(id_create_roll_s1[3][1]);

  order_roll_s2[0].push_back(id_create_roll_s2[0][nbeads-1]);
  order_roll_s2[1].push_back(id_create_roll_s2[1][nbeads-1]);
  order_roll_s2[2].push_back(id_create_roll_s2[2][nbeads-1]);
  order_roll_s2[3].push_back(id_create_roll_s2[3][nbeads-1]);










  // HOW MANY TIMES THERE ARE 3 CONSECUTIVE DENATURATED BP
  int n3consden = 0;
  for(int n=0; n<nbeads-2; n++){
      if(Denaturation[n]==0 && Denaturation[n+1]==0 && Denaturation[n+2]==0)
      {
          n3consden = n3consden + 1;
      }
        
  }
  cout <<"Number of 3 consecutive denaturated bp: " << n3consden << endl;





  // SAVE THE ID OF THE PATCHES IN THE ANGLE WITH THE 3 CONSECUTIVE DENATURATED BP

  //COSINE
  for(int n=0; n<nbeads-2; n++)
  {
      int contador7 = Denaturation[n] + Denaturation[n+1] + Denaturation[n+2]; //3 for 0 denaturated bp
                                                                               //2 for 1 denaturated bp
                                                                               //1 for 2 denaturated bp
                                                                               //0 for 3 denaturaded bp
      //NON DENATURATED COSINE (dsDNA)
      if(contador7>0)
      {   
          //FOR STRAND 1 
          id_create_cosine_s1[0].push_back (HB_order[1][n]);              //first patch
          id_create_cosine_s1[1].push_back (HB_order[1][n+1]);            //second patch
          id_create_cosine_s1[2].push_back (HB_order[1][n+2]);            //third patch
          id_create_cosine_s1[3].push_back (2);                           //type of the cosine angle

          //FOR STRAND 2
          id_create_cosine_s2[0].push_back (HB_order[2][n]);
          id_create_cosine_s2[1].push_back (HB_order[2][n+1]);
          id_create_cosine_s2[2].push_back (HB_order[2][n+2]);
          id_create_cosine_s2[3].push_back (2);                           //type of the cosine angle
      }

      //DENATURATED COSINE (ssDNA)
      if(contador7==0)
      {   
          //FOR STRAND 1 
          id_create_cosine_s1[0].push_back (HB_order[1][n]);              //first patch
          id_create_cosine_s1[1].push_back (HB_order[1][n+1]);            //second patch
          id_create_cosine_s1[2].push_back (HB_order[1][n+2]);            //third patch
          id_create_cosine_s1[3].push_back (4);                           //type of the cosine angle for denaturated bp

          //FOR STRAND 2
          id_create_cosine_s2[0].push_back (HB_order[2][n]);
          id_create_cosine_s2[1].push_back (HB_order[2][n+1]);
          id_create_cosine_s2[2].push_back (HB_order[2][n+2]);
          id_create_cosine_s2[3].push_back (4);                           //type of the cosine angle for denaturated bp
      }
  }


//for(int n=0; n<id_create_cosine_s1[0].size(); n++){cout <<"Id of the angles: " << n+1 << " " << id_create_cosine_s1[3][n] << " " << id_create_cosine_s1[0][n]<< " " << id_create_cosine_s1[1][n] << " " << id_create_cosine_s1[2][n] << endl;}

//for(int n=0; n<id_create_cosine_s2[0].size(); n++){cout <<"Id of the angles: " << n+1 << " " << id_create_cosine_s2[3][n] << " " << id_create_cosine_s2[0][n]<< " " << id_create_cosine_s2[1][n] << " " << id_create_cosine_s2[2][n] << endl;}








  /////////////////////////////////////////////////////////////////////////////
  ///////////////////	WRITE NEW FILE  ///////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////

  //OUTFILE
  stringstream writeFile;
  writeFile << outfile_new << time;
  ofstream write(writeFile.str().c_str());
  cout << "writing on .... " << writeFile.str().c_str() <<endl;
  //set precision and the number of decimals to be printed always
  write.precision(9);
  write.setf(ios::fixed);
  write.setf(ios::showpoint);


  write << "LAMMPS data file configuration linear dsDNA molecule with denaturation protocol; timestep =  "<< argv[1] << endl;
  write <<endl;

  write << Ntotal           << " atoms"       <<endl;
  write << 6                << " atom types"  <<endl;
  write << nbonds           << " bonds"       <<endl;
  write << 4                << " bond types"  <<endl;
  write << nangles          << " angles"      <<endl;
  write << 4                << " angle types" <<endl;
  write << ndihedrals       << " dihedrals"      <<endl;
  write << 2                << " dihedral types" <<endl;

  write << "\n";
  write << Lminx << " " << Lmaxx << " xlo xhi" <<endl;
  write << Lminy << " " << Lmaxy << " ylo yhi" <<endl;
  write << Lminz << " " << Lmaxz << " zlo zhi" <<endl;



  write << "\nMasses\n" <<endl;
  for(int i=0; i<ntypes;i++) 
  {  
      write << i+1 << " " << mass[i] << endl; 
  }



 
  write << "\nAtoms\n"<<endl;
  for(int i=0; i<Ntotal;i++) 
  {  
      write << Atom1[0][i] << " " << Atom1[1][i] << " " << Atom1[2][i] << " " << Atom2[0][i] << " " << Atom2[1][i] << " " << Atom2[2][i] << " " << Atom1[3][i] << " " << Atom1[4][i] << " " << Atom1[5][i] << endl; 
  }




  write << "\nVelocities\n"<<endl;
  for(int n=0; n<Ntotal;n++)
  {
       write << Atom1[0][n] << " " << Velocity[0][n] << " "<< Velocity[1][n] << " "<<  Velocity[2][n] << endl;
  }



  write << "\nBonds\n"<<endl;
  //FENE BONDS (all of them)
  int cuenta=0;
  for(int n=0;n<Fene[0].size();n++)
  {
      cuenta = cuenta+1;
      write << cuenta << " " << Fene[0][n] << " "<< Fene[1][n] << " "<<  Fene[2][n] << endl;
  }

  //HB (all of them)
  for(int n=0;n<HB[0].size();n++)
  {
      cuenta = cuenta+1;
      write << cuenta << " " << HB[0][n] << " "<< HB[1][n] << " "<<  HB[2][n] << endl;
  }

  //MORSE BONDS
 //strand1
  for(int n=0;n<id_create_stacking_s1[0].size();n++)
  {
      cuenta=cuenta+1;
      write << cuenta << " " << id_create_stacking_s1[2][n] << " " << id_create_stacking_s1[0][n] << " "<< id_create_stacking_s1[1][n] << endl;
  }   

 //strand2
  for(int n=0;n<id_create_stacking_s2[0].size();n++)
  {
      cuenta=cuenta+1;
      write << cuenta << " " << id_create_stacking_s2[2][n] << " " << id_create_stacking_s2[0][n] << " "<< id_create_stacking_s2[1][n] << endl;
  }   






  write << "\nAngles\n"<<endl;
  //ROLL ANGLES
  int conta=0;

  //strand1
  for(int n=0;n<id_create_roll_s1[0].size();n++)
  {
      conta=conta+1;
      write << conta << " " << order_roll_s1[3][n] << " " << order_roll_s1[0][n] << " "<< order_roll_s1[1][n] << " "<<  order_roll_s1[2][n] << endl;
  }      

  //strand2
  for(int n=0;n<id_create_roll_s2[0].size();n++)
  {
      conta=conta+1;
      write << conta << " " << order_roll_s2[3][n] << " " << order_roll_s2[0][n] << " "<< order_roll_s2[1][n] << " "<<  order_roll_s2[2][n] << endl;
  }      

  //COSINE ANGLES
  //strand1
  for(int n=0;n<id_create_cosine_s1[0].size();n++)
  {
      conta=conta+1;       
      write << conta << " " << id_create_cosine_s1[3][n] << " "<< id_create_cosine_s1[0][n] << " "<<  id_create_cosine_s1[1][n] << " "<<  id_create_cosine_s1[2][n] <<endl;
  }
 
  //strand2
  for(int n=0;n<id_create_cosine_s2[0].size();n++)
  {
      conta=conta+1;       
      write << conta << " " << id_create_cosine_s2[3][n] << " "<< id_create_cosine_s2[0][n] << " "<<  id_create_cosine_s2[1][n] << " "<<  id_create_cosine_s2[2][n] <<endl;
  }


  




  write << "\nDihedrals\n"<<endl;

  //DIHEDRALS
  int conta2=0;
  //strand1
  for(int n=0;n<id_create_dihedral_s1[0].size();n++)
  {
      conta2 = conta2 + 1;
      write << conta2 << " " << id_create_dihedral_s1[4][n] << " " << id_create_dihedral_s1[0][n] << " "<< id_create_dihedral_s1[1][n] << " "<< id_create_dihedral_s1[2][n]<< " "<<  id_create_dihedral_s1[3][n] << endl;
  }      

  //strand2
  for(int n=0;n<id_create_dihedral_s2[0].size();n++)
  {
      conta2 = conta2 + 1;
      write << conta2 << " " << id_create_dihedral_s2[4][n] << " " << id_create_dihedral_s2[0][n] << " "<< id_create_dihedral_s2[1][n] << " "<<  id_create_dihedral_s2[2][n]<< " "<<  id_create_dihedral_s2[3][n] << endl;
  }      





  // CLOSE WRITE FILE
  write.close();







return 0;                                                                                      
}
