# Initial configuration of the linear molecule without phosphates (for denaturation)

This produces exactly the same initial configuration as the linear chain without phosphates but this time there are:

    a) 4 types of bonds instead of 3. The extra bond is because the magnitude of the morse interaction decreases for the denatured base-pairs.
    b) 4 types of angles instead of 2. The two extra angles is because the magnitude of the *Roll* and *Kratky–Porod* interactions decreases for the denatured base-pairs.
    c) 2 types of dihedrals instead of 1. The extra dihedral is because the magnitude of the *dihedral* interaction decreases for the denatured base-pairs.

In this example run the program (./lineardsDNA_setup_denaturation_for_lammps.out) and create an initial configuration with 500 bp, 50 turns and box size in the x-y direction equal to 30.
