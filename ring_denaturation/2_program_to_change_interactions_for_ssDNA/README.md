# C++ programm to change the persistence length of the denaturated base-pairs in a ring DNA molecule

The program "*lp_ssDNA_new_list_cosine_stack_roll_dihedral_ring*" computes the distance between patches for every base pair. If this is distance is bigger than a **cuttoff** (**0.3 nm**) for two or more consecutive base-pairs, then the program changes the angle, dihedral and bond types of the dentaured base-pairs. This is done in the following way, to simulate the lack of rigidity for the ssDNA (denatured section):


|   **Non denatured base-pairs**   |     **Denatured base-pairs**    |
|----------------------------------|---------------------------------|
| angle_coeff 1 harmonic 200.0 90  | angle_coeff 3 harmonic 20.0 90  |
| angle_coeff 2 cosine 52.0        | angle_coeff 4 cosine 2.0        |
| dihedral_coeff 1 50.0 1 -144 0.0 | dihedral_coeff 2 0.0 1 -144 0.0 |
| bond_coeff 3 morse 30.0 8 0.33   | bond_coeff 4 morse 3.0 8 0.33   |

**Note**: In this folder, the "*testinitialring_0*" file contains the initial configuration of a ring molecule 10bp long with 1 helical turn and no denatured base-pairs. Therefore the file produced after using the program (./lp_ssDNA_new_list_cosine_stack_roll_dihedral_ring.out 0 testinitialring_ result__) called "*result_0*" is exactly the same.

The "*testinitialring_1*" file contains the initial configuration of a ring molecule 10bp long with 1 helical turn, where the position of patches corresponding to base-pairs 2-22, 4-24, 6-26, 14-34, 16-36, 20-40 has been modified so they are denatured. Therefore the file produced after using the program (./lp_ssDNA_new_list_cosine_stack_roll_dihedral_ring.out 1 testinitialring_ result__) called "*result_1*"  changes the type of morse, dihedral, roll and cosine of the denatured base-pairs.

**both have been succesfully tested**





