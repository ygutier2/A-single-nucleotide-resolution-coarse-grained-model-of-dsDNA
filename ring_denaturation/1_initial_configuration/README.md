# Initial configuration of the ring molecule without phosphates (for denaturation)

This produces exactly the same initial configuration as the ring molecule without phosphates but this time there are:

    a) 4 types of bonds instead of 3. The extra bond is because the magnitude of the *morse* interaction for the denatured base-pairs decreases.
    b) 4 types of angles instead of 2. The two extra angles is because the magnitude of the *Roll* and *Kratky–Porod* interactions for the denatured base-pairs decreases.
    c) 2 types of dihedrals instead of 1. The extra dihedral is because the magnitude of the *dihedral* interaction for the denatured base-pairs decreases.

In this example run the program (./ringdsDNA_for_denaturation_setup_for_lammps.out) and create an initial configuration with 500 bp, 50 turns.
