#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <vector>      
#include <unistd.h>
#include <stdio.h>     
#include <time.h>
#include <iomanip>
#include <ctime>
#include <random> //For the random position of proteins

using namespace std;

int main()
{
    int nbp;
    cout << "Number of base-pairs in the system: ";
    cin >> nbp;

    /*Total number of particles per strand*/
    int nstrand   = 3*nbp;    
    /*Total number of DNA particles in the system*/
    int particles = 6*nbp;  



    /*Number of full turns (10 bp per turn in the relaxed state)*/
    int DNApitch;
    cout << "Number of helical turns: ";
    cin >> DNApitch;  

    /*The length of DNA: nbp*0.34 = 2*pi*r. Therefore the radius of the polymer is (nbp*0.34) / (2*pi)*/
    double L2 = ((nbp*0.34) / (2*M_PI)) +20;



    int nproteins;
    cout << "Number of proteins in the system: ";
    cin >> nproteins;

    //Number of particles per protein
    int nparticlesperprotein=6;

    //Total number of protein particles in the system
    int Nproteins = nparticlesperprotein*nproteins;



    /*Position of beads in the first strand*/
    vector<vector <double> > Backbone1(3, vector<double>(nbp));  
    /*Position of patche (Hydrogen bond site) in the first strand*/
    vector< vector <double> > HB1(3, vector<double>(nbp));
    /*Position of phosphates in the first strand*/
    vector< vector <double> > Phos1(3, vector<double>(nbp));
    /*Bead type in first strand (1 and 5 ) */
    vector<int> beadtypeS1(nbp);
    /*Patch type in first strand (2)*/
    vector<int> patchtypeS1(nbp);
    /*Phosphate type in second strand (7)*/
    vector<int> phosphatetypeS1(nbp);
    /*Molecule id first strand*/
    vector<int> moleculeS1(nbp);

    /*Position of beads in the second strand*/
    vector<vector <double> > Backbone2(3, vector<double>(nbp));  
    /*Position of patches in the second strand*/
    vector< vector <double> > HB2(3, vector<double>(nbp));
    /*Position of phosphates in the second strand*/
    vector< vector <double> > Phos2(3, vector<double>(nbp));
    /*Bead type in second strand (3 and 6 ) */
    vector<int> beadtypeS2(nbp);
    /*Patch type in second strand (4)*/
    vector<int> patchtypeS2(nbp);
    /*Phosphate type in second strand (8)*/
    vector<int> phosphatetypeS2(nbp);
    /*Molecule id second strand*/
    vector<int> moleculeS2(nbp);




    /*distance between the internal center line of the torus (DNA axis) and the centre of the beads*/
    double radius = 0.6;        
    /*distance between the internal center line of the torus (DNA axis) and the centre of the patches*/
    double gap = 0.1;       
    /*distance between the external axis and the internal axis of the torus, has been fixed in a way that distance between base pairs is 0.34, like risez in the linear case*/
    double PolymerRadius = (double)(nbp)*0.34/(2.0*M_PI);      
    double phi = (1.0/(double)nbp)*2.0*M_PI*(DNApitch);
    double theta =(1.0/(double)nbp)* 2.0*M_PI;

    /*distance between DNA axis and the centre of phosphates*/
    double radphos = 1.02032024; 
    /*the angle converted into radians for the first strand phosphate*/
    double anglegroove1 = (270+65.274132205)*2.0*M_PI/360.0;
    /*the angle converted into radians for the the second strand phosphate*/
    double anglegroove2 = (270-65.274132205)*2.0*M_PI/360.0;


    int i,j,k,l;

 
/*************************************/
/*Position of particles in the system*/
/*************************************/
for(i=0; i<nbp; i++)
{
    /*First strand bead position*/
    Backbone1[0][i] = (radius*cos((double)i*phi) + PolymerRadius)*cos(theta*(double)i);
    Backbone1[1][i] = (radius*cos((double)i*phi) + PolymerRadius)*sin(theta*(double)i);
    Backbone1[2][i] = -radius*sin((double)i*phi);

    /*First strand patch position*/
    HB1[0][i] = (gap*cos((double)i*phi) + PolymerRadius)*cos(theta*(double)i);
    HB1[1][i] = (gap*cos((double)i*phi) + PolymerRadius)*sin(theta*(double)i);
    HB1[2][i] = -gap*sin((double)i*phi);

    /*First strand Phosphate positions*/
    Phos1[0][i] = (radphos*cos(anglegroove1+(double)i*phi)+ PolymerRadius)*cos(theta*(double)i);
    Phos1[1][i] = (radphos*cos(anglegroove1+(double)i*phi)+ PolymerRadius)*sin(theta*(double)i);
    Phos1[2][i] = -radphos*sin(anglegroove1+(double)i*phi);


    /*Second strand bead position (add a phase of pi)*/
    Backbone2[0][i] = (radius*cos((double)i*phi + M_PI) + PolymerRadius)*cos(theta*(double)i);
    Backbone2[1][i] = (radius*cos((double)i*phi + M_PI) + PolymerRadius)*sin(theta*(double)i);
    Backbone2[2][i] = -radius*sin((double)i*phi + M_PI);

    /*Second strand patch position (add a phase)*/
    HB2[0][i] = (gap*cos((double)i*phi + M_PI) + PolymerRadius)*cos(theta*(double)i);
    HB2[1][i] = (gap*cos((double)i*phi + M_PI) + PolymerRadius)*sin(theta*(double)i);
    HB2[2][i] = -gap*sin((double)i*phi + M_PI);

    /*Second strand Phosphate positions*/
    Phos2[0][i] = (radphos*cos(anglegroove2+(double)i*phi)+ PolymerRadius)*cos(theta*(double)i);
    Phos2[1][i] = (radphos*cos(anglegroove2+(double)i*phi)+ PolymerRadius)*sin(theta*(double)i);
    Phos2[2][i] = -radphos*sin(anglegroove2+(double)i*phi);
}
/*
for(i=0; i<nbp; i++)
{
    //cout << Backbone1[0][i] << " " << Backbone1[1][i] << " " << Backbone1[2][i] << endl;
    //cout << HB1[0][i]       << " " << HB1[1][i]       << " " << HB1[2][i]       << endl;
    //cout << Phos1[0][i]     << " " << Phos1[1][i]     << " " << Phos1[2][i]     << endl;
    //cout << Backbone2[0][i] << " " << Backbone2[1][i] << " " << Backbone2[2][i] << endl;
    //cout << HB2[0][i]       << " " << HB2[1][i]       << " " << HB2[2][i]       << endl;
    //cout << Phos2[0][i]     << " " << Phos2[1][i]     << " " << Phos2[2][i]     << endl;
}
*/






/*********************************/
/*Type of particles in the system*/
/*********************************/
//Patches
for(i=0; i<nbp; i++)
{
    patchtypeS1[i] = 2;
    patchtypeS2[i] = 4;
}


//Beads
for(i=0; i<nbp; i++)
{ 
    //Interactive beads
    if(i%3==0)
    {
        beadtypeS1[i] = 1; 
        beadtypeS2[i] = 3;
    }

    //Ghost beads
    else
    {
        beadtypeS1[i] = 5;
        beadtypeS2[i] = 6;
    }
}

//Phosphates
for(i=0; i<nbp; i++)
{
    phosphatetypeS1[i] = 7;
    phosphatetypeS2[i] = 8;
}

/*
for(i=0; i<nbp; i++)
{
    cout << i+1      << " " << beadtypeS1[i] << " " << beadtypeS2[i] << endl;
}
*/






/*************/
/*Molecule id*/
/*************/
for(i=0; i<nbp; i++)
{
    moleculeS1[i] = i+1;
    moleculeS2[i] = i+nbp+1;
} 






/***************/
/*Bonds section*/
/***************/

//Number of FENE bonds per strand. (Type 1)
int nfene=nbp;
vector< vector <int> > feneS1(2, vector<int>(nfene));
vector< vector <int> > feneS2(2, vector<int>(nfene));

//Total number of HB bonds. (Type 2)
int NHB=nbp;
vector< vector <int> > hbonds(2, vector<int>(NHB));

//Number of morse bonds (stacking) per strand. (Type 3)
int nstacking=nbp;
vector< vector <int> > stackingS1(2, vector<int>(nstacking));
vector< vector <int> > stackingS2(2, vector<int>(nstacking));

int totalbonds = 2*nfene + NHB + 2*nstacking;

for(i=0; i<nbp; i++)
{
    if(i<nbp-1)
    {
        //FENE bonds 
        feneS1[0][i] = 3*i+1;
        feneS1[1][i] = 3*i+4;

        feneS2[0][i] = 3*i+1+3*nbp;
        feneS2[1][i] = 3*i+4+3*nbp;


        //Morse bonds 
        stackingS1[0][i] = 3*i+2;
        stackingS1[1][i] = 3*i+5;

        stackingS2[0][i] = 3*i+2+3*nbp;
        stackingS2[1][i] = 3*i+5+3*nbp;

    }

    //The last one connects the two ends
    if(i==nbp-1)
    {
        //FENE bonds 
        feneS1[0][i] = 3*i+1;
        feneS1[1][i] = 1;

        feneS2[0][i] = 3*i+1+3*nbp;
        feneS2[1][i] = 1+3*nbp;


        //Morse bonds 
        stackingS1[0][i] = 3*i+2;
        stackingS1[1][i] = 2;

        stackingS2[0][i] = 3*i+2+3*nbp;
        stackingS2[1][i] = 2+3*nbp;
    }

    //cout << feneS1[0][i]     << " " << feneS1[1][i]     << endl;
    //cout << feneS2[0][i]     << " " << feneS2[1][i]     << endl;
    //cout << stackingS1[0][i] << " " << stackingS1[1][i] << endl;
    //cout << stackingS2[0][i] << " " << stackingS2[1][i] << endl;
} 


for(i=0; i<NHB; i++)
{
    //Hydrogen bonds
    hbonds[0][i] = 3*i+2;
    hbonds[1][i] = 3*i+2+3*nbp;
    //cout << hbonds[0][i] << " " << hbonds[1][i] << endl;
}






/****************/
/*Angles section*/
/****************/

//Number of patch-patch-bead angles per strand. (Type 1)
int nangle1 = nbp;
vector< vector <int> > rollS1(3, vector<int>(nangle1));
vector< vector <int> > rollS2(3, vector<int>(nangle1));

//Number of patch-patch-patch angles per strand. Related to persistence length. (Type 2)
int nangle2 = nbp;
vector< vector <int> > cosineS1(3, vector<int>(nangle2));
vector< vector <int> > cosineS2(3, vector<int>(nangle2));

//Number of patch-patch-phosphate angles per strand.  (Type 3)
int nangle3 = nbp;
vector< vector <int> > anglephosS1(3, vector<int>(nangle3));
vector< vector <int> > anglephosS2(3, vector<int>(nangle3));

int totalangles = 2*nangle1 + 2*nangle2 + 2*nangle3;

//Set the patch-patch-bead angles
for(i=0; i<nangle1; i++)
{
    if(i<nbp-1)
    {
        rollS1[0][i] = 3*i+2;
        rollS1[1][i] = 3*i+5;
        rollS1[2][i] = 3*i+4;

        rollS2[0][i] = 3*i+2+3*nbp;
        rollS2[1][i] = 3*i+5+3*nbp;
        rollS2[2][i] = 3*i+4+3*nbp;
    }

    //The angle linking the ends
    if(i==nbp-1)
    {
        rollS1[0][i] = 3*i+2;
        rollS1[1][i] = 2;
        rollS1[2][i] = 1;

        rollS2[0][i] = 3*i+2+3*nbp;
        rollS2[1][i] = 2+3*nbp;
        rollS2[2][i] = 1+3*nbp;
    }

    //cout << i+1 << " " << rollS1[0][i] << " " << rollS1[1][i] << " " << rollS1[2][i] << endl;
    //cout << i+1 << " " << rollS2[0][i] << " " << rollS2[1][i] << " " << rollS2[2][i] << endl;
}



//Set the patch-patch-patch angles
for(i=0; i<nangle2; i++)
{
    if(i<nbp-2)
    {
        cosineS1[0][i] = 3*i+2;
        cosineS1[1][i] = 3*i+5;
        cosineS1[2][i] = 3*i+8;

        cosineS2[0][i] = 3*i+2+3*nbp;
        cosineS2[1][i] = 3*i+5+3*nbp;
        cosineS2[2][i] = 3*i+8+3*nbp;
    }

    //The next two are linking the ends
    if(i==nbp-2)
    {
        cosineS1[0][i] = 3*i+2;
        cosineS1[1][i] = 3*i+5;
        cosineS1[2][i] = 2;

        cosineS2[0][i] = 3*i+2+3*nbp;
        cosineS2[1][i] = 3*i+5+3*nbp;
        cosineS2[2][i] = 2+3*nbp;
    }

    if(i==nbp-1)
    {
        cosineS1[0][i] = 3*i+2;
        cosineS1[1][i] = 2;
        cosineS1[2][i] = 5;

        cosineS2[0][i] = 3*i+2+3*nbp;
        cosineS2[1][i] = 2+3*nbp;
        cosineS2[2][i] = 5+3*nbp;
    }

    //cout << cosineS1[0][i] << " " << cosineS1[1][i] << " " << cosineS1[2][i] << endl;
    //cout << cosineS2[0][i] << " " << cosineS2[1][i] << " " << cosineS2[2][i] << endl;
}




//Set the patch-patch-phosphate angles
for(i=0; i<nangle3; i++)
{
     if(i<nbp-1)
     {
         anglephosS1[0][i] = 3*i+2;
         anglephosS1[1][i] = 3*i+5;
         anglephosS1[2][i] = 3*i+6;

         anglephosS2[0][i] = 3*i+2+3*nbp;
         anglephosS2[1][i] = 3*i+5+3*nbp;
         anglephosS2[2][i] = 3*i+6+3*nbp;
     }

     if(i==nbp-1)
     {
         anglephosS1[0][i] = 3*i+2;
         anglephosS1[1][i] = 2;
         anglephosS1[2][i] = 3;

         anglephosS2[0][i] = 3*i+2+3*nbp;
         anglephosS2[1][i] = 2+3*nbp;
         anglephosS2[2][i] = 3+3*nbp;

     }
     //cout << anglephosS1[0][i] << " " << anglephosS1[1][i] << " " << anglephosS1[2][i] << endl;
     //cout << anglephosS2[0][i] << " " << anglephosS2[1][i] << " " << anglephosS2[2][i] << endl;
}



/*******************/
/*Dihedrals section*/
/*******************/

//Number of bead-patch-patch-bead dihedrals per strand. (Type 1)
int ndihedral = nbp;
vector< vector <int> > dihedralS1(4, vector<int>(ndihedral));
vector< vector <int> > dihedralS2(4, vector<int>(ndihedral));

int totaldihedrals = 2*ndihedral;

for(i=0; i<ndihedral; i++)
{
    if(i<nbp-1)
    {
        dihedralS1[0][i] = 3*i+1;
        dihedralS1[1][i] = 3*i+2;
        dihedralS1[2][i] = 3*i+5;
        dihedralS1[3][i] = 3*i+4;

        dihedralS2[0][i] = 3*i+1+3*nbp;
        dihedralS2[1][i] = 3*i+2+3*nbp;
        dihedralS2[2][i] = 3*i+5+3*nbp;
        dihedralS2[3][i] = 3*i+4+3*nbp;
    }

    //The dihedral linking the ends
    if(i==nbp-1)
    {
        dihedralS1[0][i] = 3*i+1;
        dihedralS1[1][i] = 3*i+2;
        dihedralS1[2][i] = 2;
        dihedralS1[3][i] = 1;

        dihedralS2[0][i] = 3*i+1+3*nbp;
        dihedralS2[1][i] = 3*i+2+3*nbp;
        dihedralS2[2][i] = 2+3*nbp;
        dihedralS2[3][i] = 1+3*nbp;
    }

    //cout << dihedralS1[0][i] << " " << dihedralS1[1][i] << " " << dihedralS1[2][i] << " " << dihedralS1[3][i] << endl;
    //cout << dihedralS2[0][i] << " " << dihedralS2[1][i] << " " << dihedralS2[2][i] << " " << dihedralS2[3][i] << endl;
}




/*********************/
/*Create the proteins*/
/*********************/
//The proteins are rigid bodies made of patchy-beads (in this case 3 patchy-beads, i.e., six particles per protein).
//Protein-beads are 2 nm in size. Protein-patches are 0.5 nm in size. The excluded volume is chosen so the proteins fit into the major but not the minor groove.
//The distance between the centre of a protein-bead and protein-patch is (2+0.5)/2 = 1.25 nm.
//Consecutive patchy-beads in the same protein are placed to follow the helical path of one of the strands of the DNA molecule.
//The twist angle between consecutive patchy-beads in a protein is set to 20°, I tried with 36° (the twist angle of our DNA model) but the proteins didn't bind to DNA.

vector< vector <double> > protein1(3, vector<double>(nproteins));  
vector< vector <double> > proteinpatch1(3, vector<double>(nproteins));  
vector< vector <double> > protein2(3, vector<double>(nproteins));  
vector< vector <double> > proteinpatch2(3, vector<double>(nproteins));  
vector< vector <double> > protein3(3, vector<double>(nproteins));  
vector< vector <double> > proteinpatch3(3, vector<double>(nproteins));

vector<int> moleculeprotein(nproteins);

double twistprotein = 20.0*(M_PI/180);
double radprotein=1.25;


//THE X RANDOM POSITION OF THE BOTTOM PATCH OF A PROTEIN
    //generates a random seed (rd)
    random_device rdx;
    //random number generator (rng)
    mt19937 rngx(rdx());
    //produces random DECIMALS in the range from -L/2 to L/2 (inclusive)
    uniform_real_distribution<double> distrx(-(L2)+1, L2-1);

    //produces random INTEGERS in the range from -L/2 to L/2 (inclusive)
    //uniform_int_distribution<int> distrx(-(L2), L2);

//THE Y RANDOM POSITION OF THE BOTTOM PATCH OF A PROTEIN
    random_device rdy;
    mt19937 rngy(rdy());
    uniform_real_distribution<double> distry(-(L2)+1, L2-1);

//THE Z RANDOM POSITION OF THE BOTTOM PATCH OF A PROTEIN
    random_device rdz;
    mt19937 rngz(rdz());
    uniform_real_distribution<double> distrz(-(L2)+1, L2-1);

    for(i=0; i<nproteins; i++)
    {
       moleculeprotein[i] = (2*nbp)+1+i;

       double comx = distrx(rngx);
       double comy = distry(rngy);
       double comz = distrz(rngz);

      /*Protein bead 1*/
       protein1[0][i] = comx+(radprotein*cos(0));
       protein1[1][i] = comy+(radprotein*sin(0));
       protein1[2][i] = comz;

      /*Protein patch 1*/
       proteinpatch1[0][i] = comx;
       proteinpatch1[1][i] = comy;
       proteinpatch1[2][i] = comz;

      /*Protein bead 2*/
       protein2[0][i] = comx+(radprotein*cos(2.0*twistprotein));
       protein2[1][i] = comy+(radprotein*sin(2.0*twistprotein));
       protein2[2][i] = comz+(0.34*2.0);

      /*Protein patch 2*/
       proteinpatch2[0][i] = comx;
       proteinpatch2[1][i] = comy;
       proteinpatch2[2][i] = comz+(0.34*2.0);

      /*Protein bead 3*/
       protein3[0][i] = comx+(radprotein*cos(4.0*twistprotein));
       protein3[1][i] = comy+(radprotein*sin(4.0*twistprotein));
       protein3[2][i] = comz+(0.34*4.0);

      /*Protein patch 3*/
       proteinpatch3[0][i] = comx;
       proteinpatch3[1][i] = comy;
       proteinpatch3[2][i] = comz+(0.34*4.0);

    }




/*********************************/
/*Write the initial configuration*/
/*********************************/
string name1("initial_configuration_ringdsDNA_phosphates_N");
string name2("_T");
string name3("_P");

stringstream writeFile;
writeFile << name1 << nbp << name2 << DNApitch << name3 << nproteins;

ofstream write(writeFile.str().c_str());
cout << "writing on .... " << writeFile.str().c_str() <<endl;

//set precision and the number of decimals to be printed always
write.precision(9);
write.setf(ios::fixed);
write.setf(ios::showpoint);


  write << "LAMMPS data file initial configuration ring dsDNA molecule with phosphates and proteins; timestep = 0" << endl;

  write << particles+Nproteins  << " atoms"     << endl;
  write << totalbonds           << " bonds"     << endl;
  write << totalangles          << " angles"    << endl;
  write << totaldihedrals       << " dihedrals" << endl;
  write << "\n";

  write << 10 << " atom types"     << endl;
  write << 3 << " bond types"     << endl;
  write << 3 << " angle types"    << endl;
  write << 1 << " dihedral types" << endl;
  write << "\n";

  write << -L2 << " " << L2 << " xlo xhi" << endl;
  write << -L2 << " " << L2 << " ylo yhi" << endl;
  write << -L2 << " " << L2 << " zlo zhi" << endl;
  write << "\n";



  write << "Masses\n" << endl;
  for(i=0; i<10;i++)
  {
      write << i+1 << " 1" << endl;
  }
  write << "\n";


  //atom id; molecule id; atom type; x; y; z
  write << "Atoms\n" << endl;
  //First strand: 
  for(i=0; i<nbp; i++ )
  {
      //Beads
      write << 3*i+1 << " " << moleculeS1[i] << " " << beadtypeS1[i]      << " " << Backbone1[0][i] << " " << Backbone1[1][i] << " " << Backbone1[2][i] << endl;
      //Patches
      write << 3*i+2 << " " << moleculeS1[i] << " " << patchtypeS1[i]     << " " << HB1[0][i]       << " " << HB1[1][i]       << " " << HB1[2][i]       << endl;
      //Phosphates
      write << 3*i+3 << " " << moleculeS1[i] << " " << phosphatetypeS1[i] << " " << Phos1[0][i]     << " " << Phos1[1][i]     << " " << Phos1[2][i]     << endl;
  } 

  //Second strand:
  for(i=0; i<nbp; i++ )
  {
      //Beads
      write << 3*i+1+3*nbp << " " << moleculeS2[i] << " " << beadtypeS2[i]      << " " << Backbone2[0][i] << " " << Backbone2[1][i] << " " << Backbone2[2][i] << endl;
      //Patches
      write << 3*i+2+3*nbp << " " << moleculeS2[i] << " " << patchtypeS2[i]     << " " << HB2[0][i]       << " " << HB2[1][i]       << " " << HB2[2][i]       << endl;
      //Phosphates
      write << 3*i+3+3*nbp << " " << moleculeS2[i] << " " << phosphatetypeS2[i] << " " << Phos2[0][i]     << " " << Phos2[1][i]     << " " << Phos2[2][i]     << endl;
  }
  //Proteins:
  for (i=0; i<nproteins;i++)
  {
      //Protein bead 1
      write << (nparticlesperprotein*i)+1+particles << " " << moleculeprotein[i] << " " << 9  << " " << protein1[0][i]      << " " << protein1[1][i]      << " " << protein1[2][i]      << endl;
      //Protein Patch 1
      write << (nparticlesperprotein*i)+2+particles << " " << moleculeprotein[i] << " " << 10 << " " << proteinpatch1[0][i] << " " << proteinpatch1[1][i] << " " << proteinpatch1[2][i] << endl;
      //Protein bead 2
      write << (nparticlesperprotein*i)+3+particles << " " << moleculeprotein[i] << " " << 9  << " " << protein2[0][i]      << " " << protein2[1][i]      << " " << protein2[2][i]      << endl;
      //Protein Patch 2
      write << (nparticlesperprotein*i)+4+particles << " " << moleculeprotein[i] << " " << 10 << " " << proteinpatch2[0][i] << " " << proteinpatch2[1][i] << " " << proteinpatch2[2][i] << endl;
      //Protein bead 3
      write << (nparticlesperprotein*i)+5+particles << " " << moleculeprotein[i] << " " << 9  << " " << protein3[0][i]      << " " << protein3[1][i]      << " " << protein3[2][i]      << endl;
      //Protein Patch 3
      write << (nparticlesperprotein*i)+6+particles << " " << moleculeprotein[i] << " " << 10 << " " << proteinpatch3[0][i] << " " << proteinpatch3[1][i] << " " << proteinpatch3[2][i] << endl;
   }
   write << "\n";





  // atomid; vx; vy; vz
  write << "Velocities\n" << endl;
  for(i=0; i<particles+Nproteins; i++ )
  {
      write << i+1 << " 0 0 0" << endl;
  } 
  write << "\n";






  // bondid; bondtype; b1; b2
  write << "Bonds\n" << endl;

  int bondid =0;
  // FENE bonds S1 (type 1)
  for(i=0; i<nfene; i++ )
  {
      bondid = bondid+1;
      write << bondid << " " << 1 << " " << feneS1[0][i] << " " << feneS1[1][i] << endl;
  }
  // FENE bonds S2 (type 1)
  for(i=0; i<nfene; i++ )
  {
      bondid = bondid+1;
      write << bondid << " " << 1 << " " << feneS2[0][i] << " " << feneS2[1][i] << endl;
  }  

  // Hydrogen bonds (type 2)
  for(i=0; i<NHB; i++ )
  {
      bondid = bondid+1;
      write << bondid << " " << 2 << " " << hbonds[0][i] << " " << hbonds[1][i] << endl;
  }

  // MORSE bonds S1 (type 3)
  for(i=0; i<nstacking; i++ )
  {
      bondid = bondid+1;
      write << bondid << " " << 3 << " " << stackingS1[0][i] << " " << stackingS1[1][i] << endl;
  }
  // MORSE bonds S2 (type 3)
  for(i=0; i<nstacking; i++ )
  {
      bondid = bondid+1;
      write << bondid << " " << 3 << " " << stackingS2[0][i] << " " << stackingS2[1][i] << endl;
  }
  write << "\n";





  // angleid; angletype; a1; a2; a3
  write << "Angles\n" << endl;

  int angleid =0;
  // Roll angle S1   (type 1)
  for(i=0; i<nangle1; i++ )
  {
      angleid = angleid+1;
      write << angleid << " " << 1 << " " << rollS1[0][i] << " " << rollS1[1][i] << " " << rollS1[2][i] << endl;
  }
  // Roll angle S2   (type 1)
  for(i=0; i<nangle1; i++ )
  {
      angleid = angleid+1;
      write << angleid << " " << 1 << " " << rollS2[0][i] << " " << rollS2[1][i] << " " << rollS2[2][i] << endl;
  } 
  // Cosine angle S1 (type 2)
  for(i=0; i<nangle2; i++ )
  {
      angleid = angleid+1;
      write << angleid << " " << 2 << " " << cosineS1[0][i] << " " << cosineS1[1][i] << " " << cosineS1[2][i] << endl;
  }
  // Cosine angle S2 (type 2)
  for(i=0; i<nangle2; i++ )
  {
      angleid = angleid+1;
      write << angleid << " " << 2 << " " << cosineS2[0][i] << " " << cosineS2[1][i] << " " << cosineS2[2][i] << endl;
  }
  // Patch-Patch-Phosphate angle S1   (type 3)
  for(i=0; i<nangle3; i++ )
  {
      angleid = angleid+1;
      write << angleid << " " << 3 << " " << anglephosS1[0][i] << " " << anglephosS1[1][i] << " " << anglephosS1[2][i] << endl;
  }
  // Patch-Patch-Phosphate angle S2   (type 3)
  for(i=0; i<nangle3; i++ )
  {
      angleid = angleid+1;
      write << angleid << " " << 3 << " " << anglephosS2[0][i] << " " << anglephosS2[1][i] << " " << anglephosS2[2][i] << endl;
  } 

  write << "\n";




  // dihedralid; dihedraltype; d1; d2; d3; d4
  write << "Dihedrals\n" << endl;

  int dihedralid =0;
  // Dihedral S1 (type 1)
  for(i=0; i<ndihedral; i++ )
  {
      dihedralid = dihedralid+1;
      write << dihedralid << " " << 1 << " " << dihedralS1[0][i] << " " << dihedralS1[1][i] << " " << dihedralS1[2][i] << " " << dihedralS1[3][i] << endl;
  }
  // Dihedral S2 (type 1)
  for(i=0; i<ndihedral; i++ )
  {
      dihedralid = dihedralid+1;
      write << dihedralid << " " << 1 << " " << dihedralS2[0][i] << " " << dihedralS2[1][i] << " " << dihedralS2[2][i] << " " << dihedralS2[3][i] << endl;
  }

return 0; 
}

