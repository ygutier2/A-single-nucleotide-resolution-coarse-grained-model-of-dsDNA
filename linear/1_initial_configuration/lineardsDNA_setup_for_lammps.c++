#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <vector>      
#include <unistd.h>
#include <stdio.h>     
#include <time.h>
#include <iomanip>
#include <ctime>

using namespace std;

int main()
{
    int nbp;
    cout << "Number of base-pairs in the system: ";
    cin >> nbp;


    /*Total number of particles per strand*/
    int nstrand   = 2*nbp;    
    /*Total number of particles in the system*/
    int particles = 4*nbp;  


    /*Number of full turns (10 bp per turn in the relaxed state)*/
    int DNApitch;
    cout << "Number of helical turns: ";
    cin >> DNApitch;  

    /*The box size along the z direction is from -10 to nbp*0.34*/
    /*In the x and y directions the user introduce the half of the length*/
    int L2;
    cout << "Box size (in the x-y direction) L/2: ";
    cin >> L2; 


    /*Position of beads in the first strand*/
    vector<vector <double> > Backbone1(3, vector<double>(nbp));  
    /*Position of patche (Hydrogen bond site) in the first strand*/
    vector< vector <double> > HB1(3, vector<double>(nbp));
    /*Bead type in first strand (1 and 5 ) */
    vector<int> beadtypeS1(nbp);
    /*Patch type in first strand (2)*/
    vector<int> patchtypeS1(nbp);
    /*Molecule id first strand*/
    vector<int> moleculeS1(nbp);

    /*Position of beads in the second strand*/
    vector<vector <double> > Backbone2(3, vector<double>(nbp));  
    /*Position of patches in the second strand*/
    vector< vector <double> > HB2(3, vector<double>(nbp));
    /*Bead type in second strand (3 and 6 ) */
    vector<int> beadtypeS2(nbp);
    /*Patch type in second strand (4)*/
    vector<int> patchtypeS2(nbp);
    /*Molecule id second strand*/
    vector<int> moleculeS2(nbp);



    /*DNA rise along z axis*/
    double risez = 0.34;
    /*distance between DNA axis and the centre of the beads*/
    double radius =  0.5612310241546864907; 
    /*distance between DNA axis and patches*/    
    double gap = 0.0;                

    int i,j,k,l;

 
/*************************************/
/*Position of particles in the system*/
/*************************************/
for(i=0; i<nbp; i++)
{
    /*First strand bead position*/
    Backbone1[0][i] = radius*cos(((double)i*1.0/(double)nbp)*2*M_PI*(DNApitch));
    Backbone1[1][i] = radius*sin(((double)i*1.0/(double)nbp)*2*M_PI*(DNApitch));
    Backbone1[2][i] = (double)i*risez;

    /*First strand patch position*/
    HB1[0][i] = gap*cos(((double)i*1.0/(double)nbp)*2*M_PI*(DNApitch));
    HB1[1][i] = gap*sin(((double)i*1.0/(double)nbp)*2*M_PI*(DNApitch));
    HB1[2][i] = (double)i*risez;


    /*Second strand bead position (add a phase of pi)*/
    Backbone2[0][i] = radius*cos(((double)i*1.0/(double)nbp)*2*M_PI*(DNApitch) + M_PI);
    Backbone2[1][i] = radius*sin(((double)i*1.0/(double)nbp)*2*M_PI*(DNApitch) + M_PI);
    Backbone2[2][i] = (double)i*risez;

    /*Second strand patch position (add a phase)*/
    HB2[0][i] = gap*cos(((double)i*1.0/(double)nbp)*2*M_PI*(DNApitch) + M_PI);
    HB2[1][i] = gap*sin(((double)i*1.0/(double)nbp)*2*M_PI*(DNApitch) + M_PI);
    HB2[2][i] = (double)i*risez;
}
/*
for(i=0; i<nbp; i++)
{
    //cout << Backbone1[0][i] << " " << Backbone1[1][i] << " " << Backbone1[2][i] << endl;
    //cout << HB1[0][i]       << " " << HB1[1][i]       << " " << HB1[2][i]       << endl;
    //cout << Backbone2[0][i] << " " << Backbone2[1][i] << " " << Backbone2[2][i] << endl;
    cout << HB2[0][i]       << " " << HB2[1][i]       << " " << HB2[2][i]       << endl;
}
*/






/*********************************/
/*Type of particles in the system*/
/*********************************/
//Patches
for(i=0; i<nbp; i++)
{
    patchtypeS1[i] = 2;
    patchtypeS2[i] = 4;
}


//Beads
for(i=0; i<nbp; i++)
{ 
    //Interactive beads
    if(i%3==0)
    {
        beadtypeS1[i] = 1; 
        beadtypeS2[i] = 3;
    }

    //Ghost beads
    else
    {
        beadtypeS1[i] = 5;
        beadtypeS2[i] = 6;
    }
}
/*
for(i=0; i<nbp; i++)
{
    cout << i+1      << " " << beadtypeS1[i] << " " << beadtypeS2[i] << endl;
}
*/






/*************/
/*Molecule id*/
/*************/
for(i=0; i<nbp; i++)
{
    moleculeS1[i] = i+1;
    moleculeS2[i] = i+nbp+1;
} 






/***************/
/*Bonds section*/
/***************/

//Number of FENE bonds per strand. (Type 1)
int nfene=nbp-1;
vector< vector <int> > feneS1(2, vector<int>(nfene));
vector< vector <int> > feneS2(2, vector<int>(nfene));

//Total number of HB bonds. (Type 2)
int NHB=nbp;
vector< vector <int> > hbonds(2, vector<int>(NHB));

//Number of morse bonds (stacking) per strand. (Type 3)
int nstacking=nbp-1;
vector< vector <int> > stackingS1(2, vector<int>(nstacking));
vector< vector <int> > stackingS2(2, vector<int>(nstacking));

int totalbonds = 2*nfene + NHB + 2*nstacking;

for(i=0; i<nbp-1; i++)
{
    //FENE bonds 
    feneS1[0][i] = 2*i+1;
    feneS1[1][i] = 2*i+3;
    //cout << feneS1[0][i] << " " << feneS1[1][i] << endl;

    feneS2[0][i] = 2*i+1+2*nbp;
    feneS2[1][i] = 2*i+3+2*nbp;
    //cout << feneS2[0][i] << " " << feneS2[1][i] << endl;


    //Morse bonds 
    stackingS1[0][i] = 2*i+2;
    stackingS1[1][i] = 2*i+4;
    //cout << stackingS1[0][i] << " " << stackingS1[1][i] << endl;

    stackingS2[0][i] = 2*i+2+2*nbp;
    stackingS2[1][i] = 2*i+4+2*nbp;
    //cout << stackingS2[0][i] << " " << stackingS2[1][i] << endl;
} 


for(i=0; i<NHB; i++)
{
    //Hydrogen bonds
    hbonds[0][i] = 2*i+2;
    hbonds[1][i] = 2*i+2+2*nbp;
    //cout << hbonds[0][i] << " " << hbonds[1][i] << endl;
}






/****************/
/*Angles section*/
/****************/

//Number of patch-patch-bead angles per strand. There is a constriction for the linear chain. (Type 1)
int nangle1 = nbp;
vector< vector <int> > rollS1(3, vector<int>(nangle1));
vector< vector <int> > rollS2(3, vector<int>(nangle1));

//Number of patch-patch-patch angles per strand. Related to persistence length. (Type 2)
int nangle2 = nbp-2;
vector< vector <int> > cosineS1(3, vector<int>(nangle2));
vector< vector <int> > cosineS2(3, vector<int>(nangle2));

int totalangles = 2*nangle1 + 2*nangle2;

//Set the patch-patch-bead angles
for(i=0; i<nangle1; i++)
{
    if(i<nbp-1)
    {
        rollS1[0][i] = 2*i+2;
        rollS1[1][i] = 2*i+4;
        rollS1[2][i] = 2*i+3;

        rollS2[0][i] = 2*i+2+2*nbp;
        rollS2[1][i] = 2*i+4+2*nbp;
        rollS2[2][i] = 2*i+3+2*nbp;
    }

    //The constraint bead-patch-patch (is at the first base-pair)
    if(i==nbp-1)
    {
        rollS1[0][i] = 1;
        rollS1[1][i] = 2;
        rollS1[2][i] = 4;

        rollS2[0][i] = 1+2*nbp;
        rollS2[1][i] = 2+2*nbp;
        rollS2[2][i] = 4+2*nbp;
    }

    //cout << i+1 << " " << rollS1[0][i] << " " << rollS1[1][i] << " " << rollS1[2][i] << endl;
    //cout << i+1 << " " << rollS2[0][i] << " " << rollS2[1][i] << " " << rollS2[2][i] << endl;
}


//Set the patch-patch-patch angles
for(i=0; i<nangle2; i++)
{
     cosineS1[0][i] = 2*i+2;
     cosineS1[1][i] = 2*i+4;
     cosineS1[2][i] = 2*i+6;
     //cout << cosineS1[0][i] << " " << cosineS1[1][i] << " " << cosineS1[2][i] << endl;

     cosineS2[0][i] = 2*i+2+2*nbp;
     cosineS2[1][i] = 2*i+4+2*nbp;
     cosineS2[2][i] = 2*i+6+2*nbp;
     //cout << cosineS2[0][i] << " " << cosineS2[1][i] << " " << cosineS2[2][i] << endl;
}





/*******************/
/*Dihedrals section*/
/*******************/

//Number of bead-patch-patch-bead dihedrals per strand. (Type 1)
int ndihedral = nbp-1;
vector< vector <int> > dihedralS1(4, vector<int>(ndihedral));
vector< vector <int> > dihedralS2(4, vector<int>(ndihedral));

int totaldihedrals = 2*ndihedral;

for(i=0; i<ndihedral; i++)
{
    dihedralS1[0][i] = 2*i+1;
    dihedralS1[1][i] = 2*i+2;
    dihedralS1[2][i] = 2*i+4;
    dihedralS1[3][i] = 2*i+3;
    //cout << dihedralS1[0][i] << " " << dihedralS1[1][i] << " " << dihedralS1[2][i] << " " << dihedralS1[3][i] << endl;

    dihedralS2[0][i] = 2*i+1+2*nbp;
    dihedralS2[1][i] = 2*i+2+2*nbp;
    dihedralS2[2][i] = 2*i+4+2*nbp;
    dihedralS2[3][i] = 2*i+3+2*nbp;
    //cout << dihedralS2[0][i] << " " << dihedralS2[1][i] << " " << dihedralS2[2][i] << " " << dihedralS2[3][i] << endl;
}



/*********************************/
/*Write the initial configuration*/
/*********************************/
string name1("initial_configuration_lineardsDNA_N");
string name2("_T");

stringstream writeFile;
writeFile << name1 << nbp << name2 << DNApitch;

ofstream write(writeFile.str().c_str());
cout << "writing on .... " << writeFile.str().c_str() <<endl;

//set precision and the number of decimals to be printed always
write.precision(9);
write.setf(ios::fixed);
write.setf(ios::showpoint);


  write << "LAMMPS data file initial configuration linear dsDNA molecule; timestep = 0" << endl;

  write << particles      << " atoms"     << endl;
  write << totalbonds     << " bonds"     << endl;
  write << totalangles    << " angles"    << endl;
  write << totaldihedrals << " dihedrals" << endl;
  write << "\n";

  write << 6 << " atom types"     << endl;
  write << 3 << " bond types"     << endl;
  write << 2 << " angle types"    << endl;
  write << 1 << " dihedral types" << endl;
  write << "\n";

  write << -L2 << " " << L2       << " xlo xhi" << endl;
  write << -L2 << " " << L2       << " ylo yhi" << endl;
  write << -10 << " " << nbp*0.34 << " zlo zhi" << endl;
  write << "\n";



  write << "Masses\n" << endl;
  for(i=0; i<6;i++)
  {
      write << i+1 << " 1" << endl;
  }
  write << "\n";


  //atom id; molecule id; atom type; x; y; z
  write << "Atoms\n" << endl;
  //First strand: 
  for(i=0; i<nbp; i++ )
  {
      //Beads
      write << 2*i+1 << " " << moleculeS1[i] << " " << beadtypeS1[i]  << " " << Backbone1[0][i] << " " << Backbone1[1][i] << " " << Backbone1[2][i] << endl;
      //Patches
      write << 2*i+2 << " " << moleculeS1[i] << " " << patchtypeS1[i] << " " << HB1[0][i]       << " " << HB1[1][i]       << " " << HB1[2][i]       << endl;
  } 

  //Second strand:
  for(i=0; i<nbp; i++ )
  {
      //Beads
      write << 2*i+1+2*nbp << " " << moleculeS2[i] << " " << beadtypeS2[i]  << " " << Backbone2[0][i] << " " << Backbone2[1][i] << " " << Backbone2[2][i] << endl;
      //Patches
      write << 2*i+2+2*nbp << " " << moleculeS2[i] << " " << patchtypeS2[i] << " " << HB2[0][i]       << " " << HB2[1][i]       << " " << HB2[2][i]       << endl;
  } 
  write << "\n";





  // atomid; vx; vy; vz
  write << "Velocities\n" << endl;
  for(i=0; i<particles; i++ )
  {
      write << i+1 << " 0 0 0" << endl;
  } 
  write << "\n";






  // bondid; bondtype; b1; b2
  write << "Bonds\n" << endl;

  int bondid =0;
  // FENE bonds S1 (type 1)
  for(i=0; i<nfene; i++ )
  {
      bondid = bondid+1;
      write << bondid << " " << 1 << " " << feneS1[0][i] << " " << feneS1[1][i] << endl;
  }
  // FENE bonds S2 (type 1)
  for(i=0; i<nfene; i++ )
  {
      bondid = bondid+1;
      write << bondid << " " << 1 << " " << feneS2[0][i] << " " << feneS2[1][i] << endl;
  }  

  // Hydrogen bonds (type 2)
  for(i=0; i<NHB; i++ )
  {
      bondid = bondid+1;
      write << bondid << " " << 2 << " " << hbonds[0][i] << " " << hbonds[1][i] << endl;
  }

  // MORSE bonds S1 (type 3)
  for(i=0; i<nstacking; i++ )
  {
      bondid = bondid+1;
      write << bondid << " " << 3 << " " << stackingS1[0][i] << " " << stackingS1[1][i] << endl;
  }
  // MORSE bonds S2 (type 3)
  for(i=0; i<nstacking; i++ )
  {
      bondid = bondid+1;
      write << bondid << " " << 3 << " " << stackingS2[0][i] << " " << stackingS2[1][i] << endl;
  }
  write << "\n";





  // angleid; angletype; a1; a2; a3
  write << "Angles\n" << endl;

  int angleid =0;
  // Roll angle S1   (type 1)
  for(i=0; i<nangle1; i++ )
  {
      angleid = angleid+1;
      write << angleid << " " << 1 << " " << rollS1[0][i] << " " << rollS1[1][i] << " " << rollS1[2][i] << endl;
  }
  // Roll angle S2   (type 1)
  for(i=0; i<nangle1; i++ )
  {
      angleid = angleid+1;
      write << angleid << " " << 1 << " " << rollS2[0][i] << " " << rollS2[1][i] << " " << rollS2[2][i] << endl;
  } 
  // Cosine angle S1 (type 2)
  for(i=0; i<nangle2; i++ )
  {
      angleid = angleid+1;
      write << angleid << " " << 2 << " " << cosineS1[0][i] << " " << cosineS1[1][i] << " " << cosineS1[2][i] << endl;
  }
  // Cosine angle S2 (type 2)
  for(i=0; i<nangle2; i++ )
  {
      angleid = angleid+1;
      write << angleid << " " << 2 << " " << cosineS2[0][i] << " " << cosineS2[1][i] << " " << cosineS2[2][i] << endl;
  }
  write << "\n";




  // dihedralid; dihedraltype; d1; d2; d3; d4
  write << "Dihedrals\n" << endl;

  int dihedralid =0;
  // Dihedral S1 (type 1)
  for(i=0; i<ndihedral; i++ )
  {
      dihedralid = dihedralid+1;
      write << dihedralid << " " << 1 << " " << dihedralS1[0][i] << " " << dihedralS1[1][i] << " " << dihedralS1[2][i] << " " << dihedralS1[3][i] << endl;
  }
  // Dihedral S2 (type 1)
  for(i=0; i<ndihedral; i++ )
  {
      dihedralid = dihedralid+1;
      write << dihedralid << " " << 1 << " " << dihedralS2[0][i] << " " << dihedralS2[1][i] << " " << dihedralS2[2][i] << " " << dihedralS2[3][i] << endl;
  }

return 0; 
}

