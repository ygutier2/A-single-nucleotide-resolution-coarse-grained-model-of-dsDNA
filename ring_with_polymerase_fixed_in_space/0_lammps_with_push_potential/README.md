# LAMMPS executable with push custom potential

The polymerase applies a force using the potential named "**push**" (this is a custom potential developed by Chris Brackley).
In order to incorporate this interaction into the LAMMPS executable:

    1.- Download the most recent version of LAMMPS (currently this has been tested using the **22-Aug-2018** version).
    2.- Unzip the tar file containing the LAMMPS distribution.
    3.- Copy and paste the files "pair_push.cpp" and  "pair_push.h" into the folder "src/" (this folder is part of the standard directories of the lammps distribution).
    4.- Make sure that the **ASPHERE**, **RIGID**, **MOLECULE** and **USER-MISC** packages are installed.
    5.- Compile the LAMMPS executable as usual (we renamed the executable as "lmp_serial_22Aug2018_push_potential").

The interaction set by the **push** potential must be between an *ellipsoid* type atom, and a *non-ellipsoid* atom. LAMMPS will throw an error if this is not the case.
The equation for the interaction is:

   U(r) = D0[ exp(-2 \alpha r) - 2*exp(-\alpha r)] for r<$rc;

and an extra force *F* is added in the direction of the z-axis of the ellipsoid atom. In LAMMPS an example input would be:

    pair_style  hybrid   lj/cut 1.12246152962189 push 1.0
    pair_modify shift yes
    pair_coeff  1 1 lj/cut 1.0 1.0 1.12246152962189
    pair_coeff  1 2 push 30.0 2.0 1.0 1.0

where type 1 are the atoms (non ellipsoids), and type 2 is the pusher polymerase. The order of the parameters is: D0, \alpha, F, rc. Some examples with a different DNA model and details of the interaction can be found in the following [link](https://www2.ph.ed.ac.uk/~cbrackle/pusher_proteins.html)

**Note**: when the polymerase is *fixed* in the 3D space this is all you need to start with the simulations. However, if you want to simulate the case when the polymerase moves along the DNA it is necessary to use a custom fix rigid (see the folder "ring_with_polymerase_moving_along_the_polymer" for details).
