#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <vector>      
#include <unistd.h>
#include <stdio.h>     
#include <time.h>
#include <iomanip>
#include <ctime>

using namespace std;

int main()
{
    int nbp;
    cout << "Number of base-pairs in the system: ";
    cin >> nbp;


    /*Total number of particles per strand*/
    int nstrand   = 2*nbp;    
    /*Total number of particles in the system*/
    int particles = 4*nbp;  


    /*Number of full turns (10 bp per turn in the relaxed state)*/
    int DNApitch;
    cout << "Number of helical turns: ";
    cin >> DNApitch;  

    /*The length of DNA: nbp*0.34 = 2*pi*r. Therefore the radius of the polymer is (nbp*0.34) / (2*pi)*/
    double L2 = ((nbp*0.34) / (2*M_PI)) +20;


    /*Position of beads in the first strand*/
    vector<vector <double> > Backbone1(3, vector<double>(nbp));  
    /*Position of patche (Hydrogen bond site) in the first strand*/
    vector< vector <double> > HB1(3, vector<double>(nbp));
    /*Bead type in first strand (1 and 5 ) */
    vector<int> beadtypeS1(nbp);
    /*Patch type in first strand (2)*/
    vector<int> patchtypeS1(nbp);
    /*Molecule id first strand*/
    vector<int> moleculeS1(nbp);

    /*Position of beads in the second strand*/
    vector<vector <double> > Backbone2(3, vector<double>(nbp));  
    /*Position of patches in the second strand*/
    vector< vector <double> > HB2(3, vector<double>(nbp));
    /*Bead type in second strand (3 and 6 ) */
    vector<int> beadtypeS2(nbp);
    /*Patch type in second strand (4)*/
    vector<int> patchtypeS2(nbp);
    /*Molecule id second strand*/
    vector<int> moleculeS2(nbp);



    /*distance between the internal center line of the torus (DNA axis) and the centre of the beads*/
    double radius = 0.6;        
    /*distance between the internal center line of the torus (DNA axis) and the centre of the patches*/
    double gap = 0.1;       
    /*distance between the external axis and the internal axis of the torus, has been fixed in a way that distance between base pairs is 0.34, like risez in the linear case*/
    double PolymerRadius = (double)(nbp)*0.34/(2.0*M_PI);      
    double phi = (1.0/(double)nbp)*2.0*M_PI*(DNApitch);
    double theta =(1.0/(double)nbp)* 2.0*M_PI;

    int i,j,k,l;

 
/*************************************/
/*Position of particles in the system*/
/*************************************/
for(i=0; i<nbp; i++)
{
    /*First strand bead position*/
    Backbone1[0][i] = (radius*cos((double)i*phi) + PolymerRadius)*cos(theta*(double)i);
    Backbone1[1][i] = (radius*cos((double)i*phi) + PolymerRadius)*sin(theta*(double)i);
    Backbone1[2][i] = -radius*sin((double)i*phi);

    /*First strand patch position*/
    HB1[0][i] = (gap*cos((double)i*phi) + PolymerRadius)*cos(theta*(double)i);
    HB1[1][i] = (gap*cos((double)i*phi) + PolymerRadius)*sin(theta*(double)i);
    HB1[2][i] = -gap*sin((double)i*phi);


    /*Second strand bead position (add a phase of pi)*/
    Backbone2[0][i] = (radius*cos((double)i*phi + M_PI) + PolymerRadius)*cos(theta*(double)i);
    Backbone2[1][i] = (radius*cos((double)i*phi + M_PI) + PolymerRadius)*sin(theta*(double)i);
    Backbone2[2][i] = -radius*sin((double)i*phi + M_PI);

    /*Second strand patch position (add a phase)*/
    HB2[0][i] = (gap*cos((double)i*phi + M_PI) + PolymerRadius)*cos(theta*(double)i);
    HB2[1][i] = (gap*cos((double)i*phi + M_PI) + PolymerRadius)*sin(theta*(double)i);
    HB2[2][i] = -gap*sin((double)i*phi + M_PI);
}
/*
for(i=0; i<nbp; i++)
{
    //cout << Backbone1[0][i] << " " << Backbone1[1][i] << " " << Backbone1[2][i] << endl;
    //cout << HB1[0][i]       << " " << HB1[1][i]       << " " << HB1[2][i]       << endl;
    //cout << Backbone2[0][i] << " " << Backbone2[1][i] << " " << Backbone2[2][i] << endl;
    cout << HB2[0][i]       << " " << HB2[1][i]       << " " << HB2[2][i]       << endl;
}
*/






/*********************************/
/*Type of particles in the system*/
/*********************************/
//Patches
for(i=0; i<nbp; i++)
{
    patchtypeS1[i] = 2;
    patchtypeS2[i] = 4;
}


//Beads
for(i=0; i<nbp; i++)
{ 
    //Interactive beads
    if(i%3==0)
    {
        beadtypeS1[i] = 1; 
        beadtypeS2[i] = 3;
    }

    //Ghost beads
    else
    {
        beadtypeS1[i] = 5;
        beadtypeS2[i] = 6;
    }
}
/*
for(i=0; i<nbp; i++)
{
    cout << i+1      << " " << beadtypeS1[i] << " " << beadtypeS2[i] << endl;
}
*/






/*************/
/*Molecule id*/
/*************/
for(i=0; i<nbp; i++)
{
    moleculeS1[i] = i+1;
    moleculeS2[i] = i+nbp+1;
} 






/***************/
/*Bonds section*/
/***************/

//Number of FENE bonds per strand. (Type 1)
int nfene=nbp;
vector< vector <int> > feneS1(2, vector<int>(nfene));
vector< vector <int> > feneS2(2, vector<int>(nfene));

//Total number of HB bonds. (Type 2)
int NHB=nbp;
vector< vector <int> > hbonds(2, vector<int>(NHB));

//Number of morse bonds (stacking) per strand. (Type 3)
int nstacking=nbp;
vector< vector <int> > stackingS1(2, vector<int>(nstacking));
vector< vector <int> > stackingS2(2, vector<int>(nstacking));

int totalbonds = 2*nfene + NHB + 2*nstacking;

for(i=0; i<nbp; i++)
{
    if(i<nbp-1)
    {
        //FENE bonds 
        feneS1[0][i] = 2*i+1;
        feneS1[1][i] = 2*i+3;

        feneS2[0][i] = 2*i+1+2*nbp;
        feneS2[1][i] = 2*i+3+2*nbp;


        //Morse bonds 
        stackingS1[0][i] = 2*i+2;
        stackingS1[1][i] = 2*i+4;

        stackingS2[0][i] = 2*i+2+2*nbp;
        stackingS2[1][i] = 2*i+4+2*nbp;

    }

    //The last one connects the two ends
    if(i==nbp-1)
    {
        //FENE bonds 
        feneS1[0][i] = 2*i+1;
        feneS1[1][i] = 1;

        feneS2[0][i] = 2*i+1+2*nbp;
        feneS2[1][i] = 1+2*nbp;


        //Morse bonds 
        stackingS1[0][i] = 2*i+2;
        stackingS1[1][i] = 2;

        stackingS2[0][i] = 2*i+2+2*nbp;
        stackingS2[1][i] = 2+2*nbp;
    }

    //cout << feneS1[0][i]     << " " << feneS1[1][i]     << endl;
    //cout << feneS2[0][i]     << " " << feneS2[1][i]     << endl;
    //cout << stackingS1[0][i] << " " << stackingS1[1][i] << endl;
    //cout << stackingS2[0][i] << " " << stackingS2[1][i] << endl;
} 


for(i=0; i<NHB; i++)
{
    //Hydrogen bonds
    hbonds[0][i] = 2*i+2;
    hbonds[1][i] = 2*i+2+2*nbp;
    //cout << hbonds[0][i] << " " << hbonds[1][i] << endl;
}






/****************/
/*Angles section*/
/****************/

//Number of patch-patch-bead angles per strand. (Type 1)
int nangle1 = nbp;
vector< vector <int> > rollS1(3, vector<int>(nangle1));
vector< vector <int> > rollS2(3, vector<int>(nangle1));

//Number of patch-patch-patch angles per strand. Related to persistence length. (Type 2)
int nangle2 = nbp;
vector< vector <int> > cosineS1(3, vector<int>(nangle2));
vector< vector <int> > cosineS2(3, vector<int>(nangle2));


int totalangles = 2*nangle1 + 2*nangle2;

//Set the patch-patch-bead angles
for(i=0; i<nangle1; i++)
{
    if(i<nbp-1)
    {
        rollS1[0][i] = 2*i+2;
        rollS1[1][i] = 2*i+4;
        rollS1[2][i] = 2*i+3;

        rollS2[0][i] = 2*i+2+2*nbp;
        rollS2[1][i] = 2*i+4+2*nbp;
        rollS2[2][i] = 2*i+3+2*nbp;
    }

    //The angle linking the ends
    if(i==nbp-1)
    {
        rollS1[0][i] = 2*i+2;
        rollS1[1][i] = 2;
        rollS1[2][i] = 1;

        rollS2[0][i] = 2*i+2+2*nbp;
        rollS2[1][i] = 2+2*nbp;
        rollS2[2][i] = 1+2*nbp;
    }

    //cout << i+1 << " " << rollS1[0][i] << " " << rollS1[1][i] << " " << rollS1[2][i] << endl;
    //cout << i+1 << " " << rollS2[0][i] << " " << rollS2[1][i] << " " << rollS2[2][i] << endl;
}



//Set the patch-patch-patch angles
for(i=0; i<nangle2; i++)
{
    if(i<nbp-2)
    {
        cosineS1[0][i] = 2*i+2;
        cosineS1[1][i] = 2*i+4;
        cosineS1[2][i] = 2*i+6;

        cosineS2[0][i] = 2*i+2+2*nbp;
        cosineS2[1][i] = 2*i+4+2*nbp;
        cosineS2[2][i] = 2*i+6+2*nbp;
    }

    //The next two are linking the ends
    if(i==nbp-2)
    {
        cosineS1[0][i] = 2*i+2;
        cosineS1[1][i] = 2*i+4;
        cosineS1[2][i] = 2;

        cosineS2[0][i] = 2*i+2+2*nbp;
        cosineS2[1][i] = 2*i+4+2*nbp;
        cosineS2[2][i] = 2+2*nbp;
    }

    if(i==nbp-1)
    {
        cosineS1[0][i] = 2*i+2;
        cosineS1[1][i] = 2;
        cosineS1[2][i] = 4;

        cosineS2[0][i] = 2*i+2+2*nbp;
        cosineS2[1][i] = 2+2*nbp;
        cosineS2[2][i] = 4+2*nbp;
    }

    //cout << cosineS1[0][i] << " " << cosineS1[1][i] << " " << cosineS1[2][i] << endl;
    //cout << cosineS2[0][i] << " " << cosineS2[1][i] << " " << cosineS2[2][i] << endl;
}





/*******************/
/*Dihedrals section*/
/*******************/

//Number of bead-patch-patch-bead dihedrals per strand. (Type 1)
int ndihedral = nbp;
vector< vector <int> > dihedralS1(4, vector<int>(ndihedral));
vector< vector <int> > dihedralS2(4, vector<int>(ndihedral));

int totaldihedrals = 2*ndihedral;

for(i=0; i<ndihedral; i++)
{
    if(i<nbp-1)
    {
        dihedralS1[0][i] = 2*i+1;
        dihedralS1[1][i] = 2*i+2;
        dihedralS1[2][i] = 2*i+4;
        dihedralS1[3][i] = 2*i+3;

        dihedralS2[0][i] = 2*i+1+2*nbp;
        dihedralS2[1][i] = 2*i+2+2*nbp;
        dihedralS2[2][i] = 2*i+4+2*nbp;
        dihedralS2[3][i] = 2*i+3+2*nbp;
    }

    //The dihedral linking the ends
    if(i==nbp-1)
    {
        dihedralS1[0][i] = 2*i+1;
        dihedralS1[1][i] = 2*i+2;
        dihedralS1[2][i] = 2;
        dihedralS1[3][i] = 1;

        dihedralS2[0][i] = 2*i+1+2*nbp;
        dihedralS2[1][i] = 2*i+2+2*nbp;
        dihedralS2[2][i] = 2+2*nbp;
        dihedralS2[3][i] = 1+2*nbp;
    }

    //cout << dihedralS1[0][i] << " " << dihedralS1[1][i] << " " << dihedralS1[2][i] << " " << dihedralS1[3][i] << endl;
    //cout << dihedralS2[0][i] << " " << dihedralS2[1][i] << " " << dihedralS2[2][i] << " " << dihedralS2[3][i] << endl;
}







/********************/
/*Polymerase section*/
/********************/

/*The position of the centre of the cross-bar should be between the middle point of bp 5 (particles 9 and 10 in S1; and particles 2009 and 2010 in S2)
 and the middle point of bp 6 (particles 11 and 12 in S1; and particles 2011 and 2012 in S2)*/
    double cx, cy, cz;
//We compute the middle point of bp5, then we compute the middle point of bp6, then we compute the vector linking these two point (from bp5 to bp6). We devide the found vector by two and finally we add the original position of the middle point of bp5.
    cx = (((HB1[0][5] + HB2[0][5])/2.0)-((HB1[0][4] + HB2[0][4])/2.0))/2.0 + (HB1[0][4] + HB2[0][4])/2.0;
    cy = (((HB1[1][5] + HB2[1][5])/2.0)-((HB1[1][4] + HB2[1][4])/2.0))/2.0 + (HB1[1][4] + HB2[1][4])/2.0;
    cz = (((HB1[2][5] + HB2[2][5])/2.0)-((HB1[2][4] + HB2[2][4])/2.0))/2.0 + (HB1[2][4] + HB2[2][4])/2.0;

/*We surround that point with a ring liying on the x-z plane*/
    //number of beads in the ring surrounding the crossbar
    int nring=10;
    //If the radius of the bead is 1nm, the centre of this bead should be located at least 
    //1nm (DNA radius) + 0.5mn (Bead radius) = 1.5 nm apart from the centre of the DNA                                             
    double radiuspol =1.5; 
    double thetapol  =(1.0/(double)nring)*2.0*M_PI;
    vector< vector <double> > ring(3, vector<double>(nring));

    for(i=0; i<nring; i++)
    {
        ring[0][i]=cx+(radiuspol*cos(thetapol*(double)i));
        ring[1][i]=cy;
        ring[2][i]=cz+(radiuspol*sin(thetapol*(double)i));
    }

    //We add the cross bar along the vector pointing on the x direction and which centre is located at the position (cx, cy, cz)
    int nbar = 7;
    vector< vector <double> > crossbar(3, vector<double>(nbar));
    for(i=0; i<nbar; i++)
    {
        crossbar[0][i]=cx+(0.34*(i-3));
        crossbar[1][i]=cy;
        crossbar[2][i]=cz;
    }

    //Finally the ellipsoid (polymerase centre) orientation is especified as a quaternion q=(q0,q1,q2,q3). Which can be written as a rotation q=cos(theta/2) + [ux i + uy j + uz k]*sin(theta/2)
    //where (ux, uy, uz) is an unitary vector and theta is an angle roration about that vector. The push pairwise interactions adds a force to the z component of the ellipsoid. We want this component to be
    //parallel to the vector perpendicular to the plane where the ring lies (perpendicular to the xz plane, that means j). To do this we need to rotate the ellipsoid 90 degrees about the x axis. Meaning that
    //the quaternion has the form q = cos(pi/4) + [1,0,0]*sin(pi/4) = 0.707106, 0.707106, 0, 0
    double q0 = 0.707106;
    double q1 = 0.707106;
    double q2 = 0.0;
    double q3 = 0.0;





/*********************************/
/*Write the initial configuration*/
/*********************************/
string name1("initial_configuration_ringdsDNA_with_polymerase_N");
string name2("_T");

stringstream writeFile;
writeFile << name1 << nbp << name2 << DNApitch;

ofstream write(writeFile.str().c_str());
cout << "writing on .... " << writeFile.str().c_str() <<endl;

//set precision and the number of decimals to be printed always
write.precision(9);
write.setf(ios::fixed);
write.setf(ios::showpoint);


  write << "LAMMPS data file initial configuration ring dsDNA molecule; timestep = 0" << endl;

  write << particles+1+nring+nbar    << " atoms"      << endl;
  write << 1                         << " ellipsoids" << endl;
  write << totalbonds                << " bonds"     << endl;
  write << totalangles               << " angles"    << endl;
  write << totaldihedrals            << " dihedrals" << endl;
  write << "\n";

  write << 9 << " atom types"     << endl;
  write << 3 << " bond types"     << endl;
  write << 2 << " angle types"    << endl;
  write << 1 << " dihedral types" << endl;
  write << "\n";

  write << -L2 << " " << L2       << " xlo xhi" << endl;
  write << -L2 << " " << L2       << " ylo yhi" << endl;
  write << -L2 << " " << L2 << " zlo zhi" << endl;
  write << "\n";



  write << "Masses\n" << endl;
  //mass of the particles in the polymerase (m=1 for type 7, 8 and 9)
  int mcp = 1;
  for(i=0; i<9;i++)
  {
      write << i+1 << " 1" << endl;
  }
  write << "\n";


  //For style hybrid:          atom-ID atom-type x y z sub-style1 sub-style2
  //substyle1 is molecular:    atom-ID molecule-ID atom-type x y z              ---> so we just need to add the molecule-ID
  //substyle2 is ellipsoid:    atom-ID atom-type ellipsoidflag density x y z    ---> then you just have to add allipsoid flag and density
  //So we have in the atoms section atom-ID atom-type x y z molecule-ID ellipsoidflag density 
  write << "Atoms\n" << endl;
  //First strand: 
  for(i=0; i<nbp; i++ )
  {
      //Beads
      write << 2*i+1 << " " << beadtypeS1[i]  << " " << Backbone1[0][i] << " " << Backbone1[1][i] << " " << Backbone1[2][i] << " " << moleculeS1[i] << " " << 0 << " " << 1.0 << endl;
      //Patches
      write << 2*i+2 << " " << patchtypeS1[i] << " " << HB1[0][i]       << " " << HB1[1][i]       << " " << HB1[2][i]       << " " << moleculeS1[i] << " " << 0 << " " << 1.0 << endl;
  } 

  //Second strand:
  for(i=0; i<nbp; i++ )
  {
      //Beads
      write << 2*i+1+2*nbp << " " << beadtypeS2[i]  << " " << Backbone2[0][i] << " " << Backbone2[1][i] << " " << Backbone2[2][i] << " " << moleculeS2[i] << " " << 0 << " " << 1.0 << endl;
      //Patches
      write << 2*i+2+2*nbp << " " << patchtypeS2[i] << " " << HB2[0][i]       << " " << HB2[1][i]       << " " << HB2[2][i]       << " " << moleculeS2[i] << " " << 0 << " " << 1.0 << endl;
  } 

  //the centre of the ring (type 7) this is the only ellipsoid
  write << 1+4*nbp << " " << 7 << " " << cx << " " << cy << " " << cz << " " << 2*nbp+1 << " " << 1 << " " << mcp << endl;
  //the ring (type 8)
  for(i=0; i<nring; i++)
  {
      write << 4*nbp+2+i << " " << 8 << " " << ring[0][i] << " " << ring[1][i] << " " << ring[2][i] << " " << 2*nbp+1 << " " << 0 << " " << mcp << endl;
  }
  //the cross-bar (type 9)
  for(i=0; i<nbar; i++)
  {
      write << 4*nbp+2+nring+i << " " << 9 << " " << crossbar[0][i] << " " << crossbar[1][i] << " " << crossbar[2][i] << " " << 2*nbp+1 << " " << 0 << " " << mcp << endl;
  }
  write << "\n";



  //atom-ID = ID of atom which is an ellipsoid
  //shapex,shapey,shapez = 3 diameters of ellipsoid (distance units)
  //quatw,quati,quatj,quatk = quaternion components for orientation of atom
  write << "Ellipsoids\n" << endl;
  write << 4*nbp+1 << " " << 1.0 << " " << 1.0 << " " << 1.0 << " " << q0 << " " << q1 << " " << q2 << " " << q3 << endl;
  write << "\n";




  //For style hybrid:          atom-ID vx vy vz sub-style1 sub-style2 ... 
  //substyle1 is molecular:    atom-ID vx vy vz                           ---> so nothing else is necessary
  //substyle2 is ellipsoid:    atom-ID vx vy vz lx ly lz                  ---> we have toa add lx ly lz
  write << "Velocities\n" << endl;
  for(i=0; i<particles+1+nring+nbar; i++ )
  {
      write << i+1 << " 0 0 0 0 0 0" << endl;
  } 
  write << "\n";






  // bondid; bondtype; b1; b2
  write << "Bonds\n" << endl;

  int bondid =0;
  // FENE bonds S1 (type 1)
  for(i=0; i<nfene; i++ )
  {
      bondid = bondid+1;
      write << bondid << " " << 1 << " " << feneS1[0][i] << " " << feneS1[1][i] << endl;
  }
  // FENE bonds S2 (type 1)
  for(i=0; i<nfene; i++ )
  {
      bondid = bondid+1;
      write << bondid << " " << 1 << " " << feneS2[0][i] << " " << feneS2[1][i] << endl;
  }  

  // Hydrogen bonds (type 2)
  for(i=0; i<NHB; i++ )
  {
      bondid = bondid+1;
      write << bondid << " " << 2 << " " << hbonds[0][i] << " " << hbonds[1][i] << endl;
  }

  // MORSE bonds S1 (type 3)
  for(i=0; i<nstacking; i++ )
  {
      bondid = bondid+1;
      write << bondid << " " << 3 << " " << stackingS1[0][i] << " " << stackingS1[1][i] << endl;
  }
  // MORSE bonds S2 (type 3)
  for(i=0; i<nstacking; i++ )
  {
      bondid = bondid+1;
      write << bondid << " " << 3 << " " << stackingS2[0][i] << " " << stackingS2[1][i] << endl;
  }
  write << "\n";





  // angleid; angletype; a1; a2; a3
  write << "Angles\n" << endl;

  int angleid =0;
  // Roll angle S1   (type 1)
  for(i=0; i<nangle1; i++ )
  {
      angleid = angleid+1;
      write << angleid << " " << 1 << " " << rollS1[0][i] << " " << rollS1[1][i] << " " << rollS1[2][i] << endl;
  }
  // Roll angle S2   (type 1)
  for(i=0; i<nangle1; i++ )
  {
      angleid = angleid+1;
      write << angleid << " " << 1 << " " << rollS2[0][i] << " " << rollS2[1][i] << " " << rollS2[2][i] << endl;
  } 
  // Cosine angle S1 (type 2)
  for(i=0; i<nangle2; i++ )
  {
      angleid = angleid+1;
      write << angleid << " " << 2 << " " << cosineS1[0][i] << " " << cosineS1[1][i] << " " << cosineS1[2][i] << endl;
  }
  // Cosine angle S2 (type 2)
  for(i=0; i<nangle2; i++ )
  {
      angleid = angleid+1;
      write << angleid << " " << 2 << " " << cosineS2[0][i] << " " << cosineS2[1][i] << " " << cosineS2[2][i] << endl;
  }
  write << "\n";




  // dihedralid; dihedraltype; d1; d2; d3; d4
  write << "Dihedrals\n" << endl;

  int dihedralid =0;
  // Dihedral S1 (type 1)
  for(i=0; i<ndihedral; i++ )
  {
      dihedralid = dihedralid+1;
      write << dihedralid << " " << 1 << " " << dihedralS1[0][i] << " " << dihedralS1[1][i] << " " << dihedralS1[2][i] << " " << dihedralS1[3][i] << endl;
  }
  // Dihedral S2 (type 1)
  for(i=0; i<ndihedral; i++ )
  {
      dihedralid = dihedralid+1;
      write << dihedralid << " " << 1 << " " << dihedralS2[0][i] << " " << dihedralS2[1][i] << " " << dihedralS2[2][i] << " " << dihedralS2[3][i] << endl;
  }

return 0; 
}

