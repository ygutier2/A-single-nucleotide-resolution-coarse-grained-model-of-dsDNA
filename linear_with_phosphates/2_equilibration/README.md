Copy and paste here both the initial configuration generated before "*initial_configuration_lineardsDNA_phosphates_N1000_T100*" and the LAMMPS executable "*lmp_serial_22Aug2018*". Then run the lammps script:

    ./lmp_serial_22Aug2018 -in commands_lammps_dsDNA_with_phosphates
